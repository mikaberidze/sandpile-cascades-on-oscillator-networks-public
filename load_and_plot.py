#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  6 14:14:05 2020

@author: guga
"""
from mod_SP import Sandpile
from mod_WH import WorkHorse
from tqdm import tqdm
import matplotlib.pyplot as plt
import sys
from mod_WH import WorkHorse

params = [1000, 2000]

print("loading")
sys.stdout.flush()

sps = [0]*len(params)
for i in tqdm(range(len(params))):
    sps[i] = Sandpile.load("sp_var_p_"+str(params[i]))
    # sps[i].save("SP extract; "+sps[i].name)    #str(sps[i].ng) + '; ' + str(sps[i]))
   
    
print("plotting")
legends = []
sps[0].log_slope()
legends = ["slope -3/2"]
for sp in tqdm(sps):
    # hist_out = sp.plot_cascade_area_distr(error_split=30, log_bins=False)
    hist_out = sp.plot_cascade_area_distr(error_split=30, log_bins=True)
    # a = sp.fit_power_law(hist_out)
    # sp.fit_log_normal(hist_out)
    plt.show()
    plt.pause(0.1)
    legends.append("p = " + str(sp.name))
    # legends.append("fitted slope %.3f" % a)
    # sp.plot_cascade_size_distr(error_split=30)
    # legends.append("size for p="+str(sp.dissipation_p))


plt.xlabel("casccade area")

plt.legend(legends)
plt.title("n=1600, 3-regular; KP tpl->rnd, dc_dr=1.1;\nKM w=0, k=0.333; SP dT=1,  dissip=var; t_run=15000", fontsize=10)



# kp = KuraPile.load("kp stability check - "+str(params[-1]))
# kp.stability_test()

# sp = sps[3]
# hist_out = sp.plot_cascade_area_distr(error_split=30, log_bins=True)
# sp.fit_log_normal(hist_out)

# plt.legend(["log-normal fit for n=200",
#             "log-normal fit for n=6400", 
#             "area for n=200", "area for n=6400"])


nn = [1000, 2000,4000, 8000, 16000, 32000]
name = lambda n: f'WH WH of KM: d=3 n={n}, k=1, Δω=1'
whs = []
legends = []

for n in nn:
    wh = WorkHorse.load(name(n))
    whs.append(wh)
    wh.f_out_num=1.1
    wh.plot_single(0, fmt='.-')
    legends.append(f"n={n}")
plt.legend(legends)
# whs[0].plot_as_subplots([0])
