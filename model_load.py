#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  6 17:07:32 2021

@author: guga
"""
from mod_KP import KuraPile
from mod_SP import Sandpile
from mod_WH import WorkHorse
from tqdm import tqdm
import matplotlib.pyplot as plt
import numpy as np
import scipy


def S_theo(n, d, c):
    return c*n*d/2/(d-1)

paper_plot_prep_1()
wh = WorkHorse.load('WH of SP, d=var n=8000, c=2, p=0.0025')
wh.plot_single(0)

n = 8000
c = 2

dd = np.linspace(2.5,9,100)
SS = S_theo(n, dd, c)


plt.plot(dd, SS)
plt.ylabel('$S$', fontsize=10)
plt.title(f'd-regular random network, $N={n}$, $c={c}$', fontsize=10)
plt.xlabel('$d$', fontsize=10)

plt.legend(['theory', 'simulation'])
