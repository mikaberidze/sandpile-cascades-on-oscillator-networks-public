#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb  2 22:17:35 2020

@author: guga
"""

from common import Saveable
from tqdm import tqdm
from time import sleep
from mod_SP import Sandpile
import matplotlib.pyplot as plt
import numpy as np
import scipy
import random
import itertools
import networkx as nx


class KuraPile(Saveable):
    seed_data_labels = ["load", "tot_load", "cap", "w", "w_nn","del_w_nn", "w_all",
                         "phi", "phi_nn", "del_phi_nn", "phi_all",
                         "r", "r_i", "last_casc_dist"]
    
    def __init__(self, km, sp, ng):
        '''
        ng - NetworkGenerator object
        '''
        self.ng = ng

        km.kp = self
        sp.kp = self
        self.km = km
        self.sp = sp

        self.km_update_rule = self.randomize_km_phases_for_toppled
        self.reinforce_rule = None
        self.sp_update_rule = self.set_sp_caps_by_local_r
        self.punishing = True

        self.collecting_seed_stats = True

        self.nodes = self.ng.net.nodes()

        self.sandfall_period = 1
        self.l = 0.9
        
        self.import_km_ws()
        self.dw_ds = 0.1

        self.import_sp_caps()
        self.dcap_dr = 2
        # show cascade dynamics, when animating
        self.show_cascade_evolution = True 

    def import_km_ws(self):  # if KM w-s change after this, KP wont notice
        ''' 
        reads initial KM natural freq-s in order to tweak them around it
        '''
        self.w0 = {nd: self.km.get_w(nd) for nd in self.nodes}

    def import_sp_caps(self):  # if SP caps change after this, KP wont notice
        ''' 
        reads initial SP cap-s in order in order to tweak them around it
        '''
        self.cap0 = {nd: self.sp.get_node_cap(nd) for nd in self.nodes}

    def run(self, t_run, verbose=True, animate=False):
        ''' run SP and RK turn-by-turn and affect one anothers parameters '''
        t_init = self.km.t_final()
        dt = self.sandfall_period
        iters = int(t_run/dt)
        t_final = t_init + iters*dt
        last_cascade_nodes = []

        if animate:
            self.prepare_plotting()

        tt = np.linspace(t_init, t_final-dt, iters)
        t_last = tt[-1]
        if verbose:
            print("run kura pile")
            sleep(0.5)
            tt = tqdm(tt, position=0)
        
        for t in tt:
            #  evolve SP and affect KM
            self.sp.drop_and_casc(animate=animate, show_cascade_evolution = 
                                  self.show_cascade_evolution)
            if self.collecting_seed_stats: 
                self.collect_seed_stats(last_cascade_nodes)
                last_cascade_nodes = self.sp.failed_nodes_in_last_cascade
            self.update_km(animate)
            
            if t==t_last:
                break
            
            #  evolve KM and affect SP
            self.km.run(dt)
            self.update_sp()
            if animate:
                self.km.animate(last=dt, n=10)
            
            if self.reinforce_rule is not None:
                self.reinforce_rule()

    def run_hysteresis(self, ascending, n, timeout_endos=10, timeout_casc=50,
                       verbose=True, animate=False):
        ''' run SP and RK turn-by-turn and affect one anothers parameters '''
        dt = self.sandfall_period
        
        ss = []
        act = []

        if animate:
            self.prepare_plotting()

        if verbose:
            print("run kura pile")
            sleep(0.5)
        
        for i in range(n):
            if ascending:
                self.sp.add_grain_to_rnd_node(animate=animate)
            else:
                self.sp.remove_grain_randomly(animate=animate)
                
            for j in range(timeout_endos):
                #  evolve SP and affect KM
                self.sp.cascade(animate=animate, show_cascade_evolution = 
                                self.show_cascade_evolution, 
                                timeout=timeout_casc)
                self.update_km(animate)
                
                #  evolve KM and affect SP
                self.km.run(dt)
                self.update_sp()
                if animate:
                    self.km.animate(last=dt, n=10)
                
                if self.reinforce_rule is not None:
                    self.reinforce_rule()
                
            ss.append(self.sp.tot_load[-1])
            act.append(self.sp.get_activity())
            if verbose:
                print(f"s={self.sp.tot_load[-1]}, act={act[-1]}, iter {i} out if {n}")
        return ss, act

    def update_km(self, animate=False):
        ''' update KM by specified rule self.km_update_rule() '''
        self.km_update_rule()
        if animate:
            self.km.draw_phase_circle()

    def set_km_weights_by_sp_loads(self):
        ''' w_i := w0_i - dw_ds load_i '''
        for nd in self.nodes:
            sand = self.sp.grains[nd]
            w = self.w0[nd] - self.dw_ds * sand
            self.km.set_w(nd, w)

    def randomize_km_phases_for_toppled(self):
        failed = self.sp.failed_nodes_in_last_cascade
        for nd in failed:
            random.seed(self.ng.net.number_of_nodes()*len(self.sp.cascades)*nd)
            self.km.set_phase(nd, random.random()*2*np.pi)
        self.km.r_discr[-1] = self.km.calc_order_param()

    def update_sp(self):
        self.sp_update_rule()
        
    def set_sp_caps_by_local_r(self):
        for nd in self.nodes:
            r = self.km.get_local_order_param(nd)
            if self.punishing:
                cap = self.cap0[nd] - self.dcap_dr * (self.l - r) #punnishing
            else:
                cap = self.cap0[nd] + self.dcap_dr * r #rewarding
                
            self.sp.set_node_cap(nd, cap)
            
    def set_sp_caps_by_global_r(self):
        for nd in self.nodes:
            r = self.km.calc_order_param()
            if self.punishing:
                cap = self.cap0[nd] - self.dcap_dr * (self.l - r) #punnishing
            else:
                cap = self.cap0[nd] + self.dcap_dr * r #rewarding
            self.sp.set_node_cap(nd, cap)
            
    def set_sp_caps_discretely(self):
        ''' determine if node is synched with its neighbors T/F and reward T'''
        cutoff = 0.1*np.pi
        for nd in self.nodes:
            del_phi = self.km.get_del_phi_max(nd)
            if del_phi<cutoff:
                cap = self.cap0[nd] + self.dcap_dr
            else:
                cap = self.cap0[nd]
            self.sp.set_node_cap(nd, cap)

    def reinforce_deepen_10_nodes(self): #assuming nodes are numbered
        ''' every time system is close to DK, increase capacities of first
        10 nodes by 1 '''
        if not hasattr(self, 'reinforcement_number'):
            self.reinforcement_number = 0
        n=10
        while self.is_close_to_DK(p=0.7):
            for nd in range(n):
                self.cap0[nd] += 1
            
            if self.sp.cascade_monitor:
                self.reinforcement_number += 1
        
    def reinforce_deepen_a_node(self): #assuming nodes are numbered
        ''' every time system is close to DK, increase capacities of first
        10 nodes by 1 '''
        if not hasattr(self, 'reinforcement_number'):
            self.reinforcement_number = 0
            self.last_improved_node = 0
            
        while self.is_close_to_DK(p=0.9):
            self.cap0[self.last_improved_node%len(self.cap0)] += 1
            self.last_improved_node += 1
            
            if self.sp.cascade_monitor:
                self.reinforcement_number += 1
        
    def is_close_to_DK(self, p=0.9):
        ''' True if SP S is at least p part of MFT calculation of S_DK '''
        DK_cap = sum([(1+self.cap0[i]+int(self.dcap_dr))/2 
                      for i in range(len(self.cap0))])
        return self.sp.number_of_grains() > p*DK_cap
        
    def monitor(self, on=True):
        ''' turns SP and KM monitors on or off'''
        self.km.data_monitor = on
        self.sp.cascade_monitor = on
        
    def relax(self, t_run, verbose=True):
        ''' run KP without recording anything and then put monitoring on''' 
        self.monitor(on=False)
        if verbose: print("relaxing KP")
        self.run(t_run, verbose=verbose)
        if verbose: print("relaxing KP finished")
        self.monitor(on=True)
        self.km.reset_discrete_phases()
        
    def collect_seed_stats(self, last_cascade_nodes):
        ''' record interesting data regarding the cascade seeds 
        (both, exo and endogenous). more specifically:
            load, capacity, freq and phase of the seed;
            avg freq and phase of its neighbors (compared to total avg);
            local and global order params
        '''
        if not self.sp.cascade_monitor:
            return
        exo, endos = self.sp.cascade_seeds[-1]
        if not hasattr(self, "exo_seed_data"):
            self.exo_seed_data = []
            self.endo_seed_data = []
            # self.seeds_by_cascade = []
            
        def mpi_pi(phi):
            return (phi+np.pi)%(2*np.pi) - np.pi
        
        def get_seed_dict(nd):
            nn = list(self.ng.net.neighbors(nd))
            
            S = self.sp.tot_load[-1]
            si = int(self.sp.cap[nd]) + 1 #toppling load = floor(cap) + 1
            ci = self.sp.cap[nd]
            
            wall = np.average(list(self.km.w.values()))
            wi = self.km.get_w(nd)
            wnn = np.average(self.km.get_w(nn))
            
            phiall = mpi_pi(self.km.calc_avg_phase())
            phii = mpi_pi(self.km.get_phase(nd))
            phinn = mpi_pi(self.km.get_neighbors_avg_phase(nd))
            
            ri = self.km.get_local_order_param(nd)
            r = self.km.calc_order_param()
            
            dist = float('inf') 
            for target in last_cascade_nodes:
                dist = min(dist, 
                           nx.shortest_path_length(self.ng.net, nd, target))
            
            return {"load":si, "tot_load":S, "cap":ci,
                        "w":wi, "w_nn":wnn,"del_w_nn":wnn-wi, "w_all":wall,
                         "phi":phii, "phi_nn":phinn,
                             "del_phi_nn":mpi_pi(phinn-phii), "phi_all":phiall,
                         "r":r, "r_i":ri, 
                         "last_casc_dist": dist if dist<float('inf') else -1}
        
        exo_seed_dict = None
        if exo is not None:
            exo_seed_dict = get_seed_dict(exo)
            self.exo_seed_data.append(exo_seed_dict)
        
        endo_seed_dicts = []
        for endo in endos:
            seed_dict = get_seed_dict(endo)
            endo_seed_dicts.append(seed_dict)
            self.endo_seed_data.append(seed_dict)
        # self.seeds_by_cascade.append((exo_seed_dict, endo_seed_dicts))
        return
    
    def del_phi(self):
        ''' return average value of phase shift of cascade seeds from their
        neighbors, both for (endogenous, exogenous) seeds '''
        def mpi_pi(phi):
            return (phi+np.pi)%(2*np.pi) - np.pi
        phi_endo = np.array([sd["phi"] for sd in self.endo_seed_data])
        phi_nn_endo = np.array([sd["phi_nn"] for sd in self.endo_seed_data])
        del_endo = np.average(mpi_pi(phi_nn_endo - phi_endo))
        
        phi_exo = np.array([sd["phi"] for sd in self.exo_seed_data])
        phi_nn_exo = np.array([sd["phi_nn"] for sd in self.exo_seed_data])
        del_exo = np.average(mpi_pi(phi_nn_exo - phi_exo))
        
        return del_endo, del_exo
  
       
    def get_regime_dividers(self, save=True):
        ''' returns the time coordinates of the points dividing the DK cycle
        into DK, synchronization and build-up regimes'''
        if hasattr(self, 'start1'):
            return self.start1, self.start2, self.start3
        rr = self.km_r()
        ss = self.sp.tot_load
        T = int(1./self.sp.typical_frequency()[0])
        start1 = []
        start2 = []
        start3 = []
        
        for i in tqdm(range(len(ss))):
            if rr[i] == min(rr[
                            max(0, i-int(2*T/3)):min(len(rr)-1, i+int(2*T/3))
                        ]) and (len(start2)==0 or i-start2[-1]>2*T/3):
                start2.append(i)
        for i in start2:
            j=i
            while j+1<len(ss) and (rr[j+1]>rr[j] or rr[j]<0.9):
                j += 1
            if j<len(ss)-1:
                start3.append(j)
            j=i
            while j-1>=0 and (ss[j-1]>ss[j] or rr[j]<0.9):
                j -= 1
            if j>0:
                start1.append(j)
        
        if save:
            self.start1 = start1
            self.start2 = start2
            self.start3 = start3
            
        return start1, start2, start3 # DK, synch, buildup
    
    def get_regime_groups(self):
        ''' returns the array with regime indikators at each time instant'''
        st1, st2, st3 = self.get_regime_dividers()
        groups = []
        
        i,j,k = (0,0,0)
        while i<len(st1) and j<len(st2) and k<len(st3):
            if st1[i] < st2[j] and st1[i] < st3[k]:
                dt = min(st2[j], st3[k]) - st1[i]
                groups += [0]*dt
                i += 1
            elif st2[j] <= st1[i] and st2[j] <= st3[k]:
                dt = min(st1[i], st3[k]) - st2[j]
                groups += [1]*dt
                j += 1
            elif st3[k] <= st1[i] and st3[k] < st2[j]:
                dt = min(st1[i], st2[j]) - st3[k]
                groups += [2]*dt
                k += 1
            else:
                raise Exception(f'Something went wrong {st1[i]}, {st2[j]}, \
                                {st3[k]}')
        groups += [(groups[-1]+1)%3]*(len(self.sp.tot_load)-len(groups))
        return groups
    
    def cycle_heuristic(self):
        ''' a heuristic used to assess how periodic the DK cycle is
         -1 means not enough data (few DK dividers) 
         0-1 means average cascade didn't reduce r below 0.5
         1-2 avg r<0.5, and we assess how close are the regime dividers
         2 the dividers were very closely packed'''
        def means_and_std(tt):
            # returns means and stdev for S and r @ given times 
            rr = [self.km_r()[t] for t in tt]
            SS = [self.sp.tot_load[t] for t in tt]
            return (np.mean(SS), np.mean(rr)), (np.std(SS), np.std(rr))
        
        start1, start2, start3 = self.get_regime_dividers()
        if len(start1)+len(start2)+len(start3)<6:
            return -1
        
        ((S1, r1), (dS1, dr1)) = means_and_std(start1)
        ((S2, r2), (dS2, dr2)) = means_and_std(start2)
        ((S3, r3), (dS3, dr3)) = means_and_std(start3)
        
        if r2>0.5:
            return 1-r2
        
        S_scale = 1/(S1-S3)
        
        return 2 - (dr1+dr2+dr3 + (dS1+dS2+dS3)*S_scale)/6
        
        
    def get_regime_durations(self):
        ''' returns avg durations of three phases during the cycle '''
        start1, start2, start3 = self.get_regime_dividers()
        if start2[0] < start1[0]:
            start2 = start2[1:]
        if start3[0] < start1[0]:
            start3 = start3[1:]
        dt1 = []
        dt2 = []
        dt3 = []
        s3last = None
        for i in range(min(len(start1), len(start2), len(start3))):
            s1, s2, s3 = start1[i], start2[i], start3[i]
            if not (s1 < s2 < s3):
                print('wrong regime divider flags: s1={}, s2={}, s3={}'.format(s1,s2,s3))
            if s3last is not None:
                if s3last > s1:
                    print('wrong regime divider flags: s3last={}, s1={}'.format(s3last,s1))
            
            dt1.append(s2-s1)
            dt2.append(s3-s2)
            if s3last is not None:
                dt3.append(s1-s3last)
            
            s3last = s3
        dT = self.sandfall_period
        return np.array(dt1).mean()*dT, np.array(dt2).mean()*dT, \
                np.array(dt3).mean()*dT
            
    
    def pull_KM_data(self, trim=False):
        if trim:
            trim_start = self.sp.saturation_iter()
        else:
            trim_start = 0
        dt = self.sandfall_period
        tt = np.array(range(len(self.sp.cascades)))[trim_start:]*dt - dt
        self._KM_r = [self.km.get_order_param(t=max(0,t)) for t in tt]
        return
    
    def km_r(self):
        ''' returns array whos i-th elem is the r after i-th cascade '''
        if not hasattr(self, '_KM_r'):
            self.pull_KM_data()
        return self._KM_r
    
    def trim(self):
        self.pull_KM_data(trim=True)
        self.sp.trim()
        
    # def cut_off_KM(self):
    #     if self.km is None:
    #         return
    #     self.name = str(self)
    #     self.pull_KM_data(trim=True)
    #     self.km = None
    #     self.sp.trim()
        
    @staticmethod
    def merge(kps, verbose=False, trim=False):
        ''' merge simmilar sandpiles together '''
        chck = []
        
        print('extracting KM data')
        kps_for = tqdm(kps) if verbose else kps
        for kp in kps_for:
            if trim:
                kp.trim()
            else:
                kp.pull_KM_data()
        
        merged = kps[0]
        if verbose: print('merging SPs')
        merged.sp = Sandpile.merge([kp.sp for kp in kps], verbose=verbose)
        
        mergers = kps[1:]
        if verbose:
            print('merging KPs')
            mergers = tqdm(mergers)
        for kp in mergers:
            # check for mergebility
            for attr in chck:
                if merged.__dict__[attr] != kp.__dict__[attr]:
                    raise Exception("can't merge Sandpiles: different ", attr)
                    
            merged._KM_r = merged._KM_r + kp._KM_r
            if merged.collecting_seed_stats:
                merged.exo_seed_data = merged.exo_seed_data + kp.exo_seed_data
                merged.endo_seed_data = merged.endo_seed_data \
                                        + kp.endo_seed_data
        
        return merged
        
    def __str__(self):
        if hasattr(self, 'name'):
            return 'SP extract from KP; ' + self.name
        
        name = str(self.ng) + "; KP"
        if self.km_update_rule.__name__ == \
                self.set_km_weights_by_sp_loads.__name__:
            name +=  " dω/ds=" + str(self.dw_ds)
        elif self.km_update_rule.__name__ == \
                self.randomize_km_phases_for_toppled.__name__:
            name +=  " topple->rnd φ"
        else:
            print(self.km_update_rule.__name__)
            print(self.set_km_weights_by_sp_loads.__name__)
            print(self.randomize_km_phases_for_toppled.__name__)
            name += " sth wrong with KM<-SP connection"
        if self.sp_update_rule.__name__ == \
                self.set_sp_caps_by_global_r.__name__:
            name +=  ", r_glob"
        elif self.sp_update_rule.__name__ == \
                self.set_sp_caps_discretely.__name__:
            name += ", r_discr"
        elif self.sp_update_rule.__name__ == \
                self.set_sp_caps_by_local_r.__name__:
            name += ", r_loc"
            if self.km.get_local_order_param == \
                self.km.calc_local_order_param_our:
                name += "(our)"
            else:
                name += "(km)"
        else:
            raise Exception("km->sp coupling type not recognized")
        name += ' punishing' if self.punishing else ' rewarding'
        name +=  " dc/dr=" + str(self.dcap_dr) +\
            ", ΔT=" + str(self.sandfall_period)
            
        
        if self.reinforce_rule is not None and \
                                    hasattr(self, 'reinforcement_number'):
            name += ';\nreinforcement rule='+self.reinforce_rule.__name__+\
                ' (#={})'.format(self.reinforcement_number)
        name += ";\n" + str(self.km) + "; " + str(self.sp)
        return name
    
    
# -------------------------------------------------------------------- Plotters
    def seed_data_hist(self, x, endo_exo=(True, True), decorate=True):
        endo, exo = endo_exo
        def hist(data, label, col=None):
            try:
                d = np.diff(np.unique(data[0:min(50,len(data))])).min()
            except:
                d=1
            d = max((max(data)-min(data))/50, d)
            left_of_first_bin = min(data) - float(d)/2
            right_of_last_bin = max(data) + float(d)/2
            plt.hist(data, np.arange(left_of_first_bin, 
                                     right_of_last_bin + d, d), 
                     alpha=0.5, label=label, color=col)
            
        if endo:
            x_endo = [sd[x] for sd in self.endo_seed_data]
            hist(x_endo, "endogenous seeds", col='yellowgreen')
        if exo:
            x_exo = [sd[x] for sd in self.exo_seed_data]
            hist(x_exo, "exogenous seeds", col='dodgerblue')
            
        if decorate:
            plt.legend()
            plt.xlabel(x)
            plt.ylabel('count')
            plt.title(str(self), fontsize=10)
        
    def seed_data_hist_by_phases(self, x, endo_exo=(True, False),
                                 o_S=(7500, 9900), o_r=(0.1, 0.9),
                                 g_S=(0, 7500), g_r=(0.1, 0.9),
                                 b_S=(7500, 9900), b_r=(0.9, 1)):
        def isin(x, r):
            return r[0] <= x <= r[1]
        endo, exo = endo_exo
        def hist(data1, data2, data3):
            try:
                d = np.diff(np.unique(data1[0:min(50,len(data1))])).min()
            except:
                d=1
            d = max((max(data1)-min(data1))/50, d)
            left_of_first_bin = min(data1) - float(d)/2
            right_of_last_bin = max(data1) + float(d)/2
            plt.hist([data1, data2, data3], np.arange(left_of_first_bin, 
                                     right_of_last_bin + d, d), 
                     color = ['darkorange', 'darkgreen', 'black'],
                     alpha=1,
                     stacked=True)
                
        if endo:
            x_endo_o = [sd[x] for sd in self.endo_seed_data if 
                        isin(sd['tot_load'],o_S) and isin(sd['r'],o_r)]
            x_endo_g = [sd[x] for sd in self.endo_seed_data if 
                        isin(sd['tot_load'],g_S) and isin(sd['r'],g_r)]
            x_endo_b = [sd[x] for sd in self.endo_seed_data if 
                        isin(sd['tot_load'],b_S) and isin(sd['r'],b_r)]
            hist(x_endo_o, x_endo_g, x_endo_b)
        if exo:
            x_exo_o = [sd[x] for sd in self.exo_seed_data if 
                        isin(sd['tot_load'],o_S) and isin(sd['r'],o_r)]
            x_exo_g = [sd[x] for sd in self.exo_seed_data if 
                        isin(sd['tot_load'],g_S) and isin(sd['r'],g_r)]
            x_exo_b = [sd[x] for sd in self.exo_seed_data if 
                        isin(sd['tot_load'],b_S) and isin(sd['r'],b_r)]
            hist(x_exo_o, x_exo_g, x_exo_b)
            
            
        plt.legend()
        plt.xlabel(x)
        plt.ylabel('count')
        plt.title(str(self), fontsize=10)
        
        
    def seed_hist_by_phases(self, o_S=(7500, 9900), o_r=(0.1, 0.9),
                                  g_S=(0, 7500), g_r=(0.1, 0.9),
                                  b_S=(7500, 9900), b_r=(0.9, 1)):
        ''' histogram of seeds in different phases '''
        
        def isin(x, r):
            return r[0] <= x <= r[1]
                
        endo_o = len([1 for sd in self.endo_seed_data if 
                    isin(sd['tot_load'],o_S) and isin(sd['r'],o_r)])
        endo_g = len([1 for sd in self.endo_seed_data if 
                    isin(sd['tot_load'],g_S) and isin(sd['r'],g_r)])
        endo_b = len([1 for sd in self.endo_seed_data if 
                    isin(sd['tot_load'],b_S) and isin(sd['r'],b_r)])
    
        exo_o = len([1 for sd in self.exo_seed_data if 
                    isin(sd['tot_load'],o_S) and isin(sd['r'],o_r)])
        exo_g = len([1 for sd in self.exo_seed_data if 
                    isin(sd['tot_load'],g_S) and isin(sd['r'],g_r)])
        exo_b = len([1 for sd in self.exo_seed_data if 
                    isin(sd['tot_load'],b_S) and isin(sd['r'],b_r)])
        
        endo_o = 100*endo_o/(exo_o+endo_o)
        endo_g = 100*endo_g/(exo_g+endo_g)
        endo_b = 100*endo_b/(exo_b+endo_b)
        
        plt.bar([0,1,2], [endo_o,endo_g,endo_b], align='center', 
                color='yellowgreen', alpha = 0.5,
                label='endogenous seeds')
        
        plt.bar([0,1,2], [100-endo_o,100-endo_g,100-endo_b], align='center', 
                color='dodgerblue', alpha = 0.5, bottom=[endo_o,endo_g,endo_b],
                label='exogenous seeds')
        plt.xticks([0,1,2], ['DK','synch', 'build-up'])
        plt.show()

            
        plt.legend()
        plt.ylabel('%')
        
       
    def plot_all_seed_histos(self, seed_labels=seed_data_labels, save=False):
        for lbl in tqdm(seed_labels):
            if save:
                plt.clf()
            else:
                plt.figure()
                
            self.seed_data_hist(lbl)
            plt.pause(0.05)
            if save:
                plt.savefig('Out/'+lbl+'.png')
        plt.show()
    
    def seed_data_scatter(self, x, y, endo_exo=(True, True)):
        ''' plot the scatter plot with each cascade seed being an single point
        with its 'x' characteristic on x axis and 'y' characteristic on y.
        '''
        endo, exo = endo_exo
        legend = []
        if endo:
            x_endo = [sd[x] for sd in self.endo_seed_data]
            y_endo = [sd[y] for sd in self.endo_seed_data]
            plt.plot(x_endo, y_endo, '.', alpha=0.5, markersize=2)
            legend.append("endogenous seeds")
        if exo:
            x_exo = [sd[x] for sd in self.exo_seed_data]
            y_exo = [sd[y] for sd in self.exo_seed_data]
            plt.plot(x_exo, y_exo, '.', alpha=0.5, markersize=2)
            legend.append("exogenous seeds")
        
        plt.legend(legend)
        plt.xlabel(x)
        plt.ylabel(y)
        plt.title(str(self), fontsize=10)
        plt.show()
        
    def plot_all_seed_scatters(self, seed_labels=seed_data_labels, save=False):
        for lbl1, lbl2 in tqdm(itertools.product(seed_labels, seed_labels)):
            if lbl1>=lbl2: continue
            if save:
                plt.clf()
            else:
                plt.figure()
                
            self.seed_data_scatter(lbl1, lbl2)
            plt.pause(0.05)
            if save:
                plt.savefig('Out/'+lbl1+' vs '+lbl2+'.png')
        plt.show()
    
    def stability_test(self):
        ''' visual test for stability '''
        print("animate first 10 seconds KM")
        self.km.animate(first=10, n=100, local_colors=True)
        print("animate last 10 seconds KM")
        self.km.animate(last=10, n=100, local_colors=True)
        
        plt.figure()
        self.km.plot_r_vs_t()
        plt.figure()
        self.sp.plot_tot_sand()

    def prepare_plotting(self):
        ''' create subplots for KM and SP and pass each to each '''
        if hasattr(self,'plp'):
            self.km.plp = self.plp
            self.sp.plp = self.plp
            return
        if hasattr(self, 'fig') and plt.fignum_exists(self.fig.number):
            return
        fig, (ax1, ax2) = plt.subplots(1, 2)
        self.fig = fig
        self.km.set_plot_ax(ax1, fig)
        self.sp.set_plot_ax(ax2, fig)
        self.sp.draw()
        
    def scatter_casc_measure_vs_r(self, area_size=(False, True)):
        ''' casc measure vs r prior to cascade'''
        area, size = area_size
        if area:
            plt.scatter(self.km_r(), np.array(self.sp.cascades)[:,0],
                        s=1, label='cascade area')
        if size:
            plt.scatter(self.km_r(), np.array(self.sp.cascades)[:,1], 
                        s=1, label='cascade size')
        plt.ylabel('cascade measure')
        plt.xlabel('KM order parameter prior to the cascade')
        plt.legend()
        plt.title(str(self), fontsize=10)
        
    def scatter_endo_seeds_vs_r(self):
        ''' # of endo seeds vs r prior to the cascade '''
        endo_seeds = np.array(self.sp.cascade_seeds)[:,1]
        endo_seeds_num = [len(seeds) for seeds in endo_seeds]
        plt.scatter(self.km_r(), endo_seeds_num, s=1)
        plt.xlabel('KM order parameter prior to the cascade')
        plt.ylabel('# of endogenous seeds')
        plt.title(str(self), fontsize=10)
        
    def scatter_last_vs_next_casc_with_r_min(self, r_min=0.1, r_max=1, S_min=0,
                                             area_size=(False, True),
                                             fit_power=False, color=None):
        ''' cascade area/size discrete dynamics. r is after the next casc '''
        area, size = area_size
        cascs = np.array(self.sp.cascades)
        rr = self.km_r()[2:]
        s = 10
        if area:
            xx0 = cascs[:-1,0]
            yy0 = cascs[1:,0]
            xx = [x for x,r in zip(xx0,rr) if r_max>r>r_min]
            yy = [y for y,r in zip(yy0,rr) if r_max>r>r_min]
            plt.scatter(xx, yy, s=s, c=color,
                        label='areas for cascades with r>{}'.format(r_min))
            if fit_power:
                self.fit_power_law(xx, yy)
        if size:
            xx0 = cascs[:-1,1]
            yy0 = cascs[1:,1]
            xx = [x for x,r in zip(xx0,rr) if r_max>r>r_min]
            yy = [y for y,r in zip(yy0,rr) if r_max>r>r_min]
            plt.scatter(xx, yy, s=s, c=color,
                        label='sizes for cascades with\
                        \nr>{} after last casc'.format(r_min))
            if fit_power:
                self.fit_power_law(xx, yy)
        plt.xlabel('size of the current cascade')
        plt.ylabel('size of the next cascade')
        plt.title(str(self), fontsize=10)
    
    def scatter_last_vs_next_casc_by_regimes(self, area_size=(False, True)):
        ''' cascade area/size discrete dynamics. r is after the next casc '''
        area, size = area_size
        cascs = np.array(self.sp.cascades)
        groups = self.get_regime_groups()
        groups = groups[3:]+ [2,2]
        cdict = {0:'orange', 1:'darkgreen', 2:'black'}
        
        if area:
            xx = cascs[:-1,0]
            yy = cascs[1:,0]
            for g in np.unique(groups):
                ix = np.where(groups == g)
                plt.scatter(xx[ix], yy[ix], c=cdict[g], label=g, s=50)

        if size:
            xx = cascs[:-1,1]
            yy = cascs[1:,1]
            for g in np.unique(groups):
                ix = np.where(groups == g)
                plt.scatter(xx[ix], yy[ix], c=cdict[g], label=g, s=50)

        plt.xlabel('last cascade')
        plt.ylabel('next cascade')
        plt.legend()
        plt.title(str(self), fontsize=10)
        
    def fit_power_law(self, x, y):
        
        def func(x, a):
            return (np.array(x)**(1-1.65)+a)**(1/(1-1.65))
        
        popt, pcov = scipy.optimize.curve_fit(func, x, y)
        a = popt[0]
        
        plt.plot(x, func(x,a), '--', linewidth=1, label='x^({:.3f})'.format(a))
        
        return a

    def scatter_last_vs_next_casc_with_colors(self, area_size=(False, True)):
        ''' cascade area/size discrete dynamics. colored by r'''
        cm = plt.cm.get_cmap('RdYlBu')
        area, size = area_size
        cascs = np.array(self.sp.cascades)
        rr = self.km_r()
        if area:
            xx = cascs[:-1,0]
            yy = cascs[1:,0]
            sc = plt.scatter(xx, yy, c=rr, s=1, cmap=cm,
                        label='cascade area')
        if size:
            xx = cascs[:-1,1]
            yy = cascs[1:,1]
            sc = plt.scatter(xx, yy, marker='+', c=rr[1:], s=1, cmap=cm,
                        label='cascade size')
        plt.colorbar(sc).set_label('order param r')
        plt.xlabel('last cascade')
        plt.ylabel('next cascade')
        plt.legend()
        plt.title(str(self), fontsize=10)
        
    def draw_r_vs_tot_load(self, color=None, include_dividers=False, 
                           color_by_phase=False, ignore_first=0, thickness=.1):
        rr = np.array(self.km_r())[ignore_first:]
        ss = np.array(self.sp.tot_load)[ignore_first:]
        
        plt.plot(rr, ss, markersize=0.1, linewidth=thickness, color=color)
        
        plt.show()
        plt.title(str(self), fontsize=10)
        plt.xlabel('KM order parameter r')
        plt.ylabel('SP total load')
        if include_dividers:
            self.draw_regime_dividers()
        if color_by_phase:
            cdict = {0:'orange', 1:'darkgreen', 2:'black'}
            groups = self.get_regime_groups()[ignore_first:]
            for g in np.unique(groups):
                ix = np.where(groups == g)
                plt.scatter(rr[ix], ss[ix], c=cdict[g], label=g, s=1)
    
    
    def draw_regime_dividers(self):
        rr = self.km_r()
        ss = self.sp.tot_load
        start1, start2, start3 = self.get_regime_dividers()
        def plot_one(start, col):
            sss = [ss[i] for i in start]
            rrr = [rr[i] for i in start]
            plt.scatter(rrr, sss, c=col, s=1, zorder=10)
        plot_one(start1, 'orange')
        plot_one(start2, 'darkgreen')
        plot_one(start3, 'black')
        
    
    def plot_r_and_tot_load(self):
        rr = self.km_r()
        ss = np.array(self.sp.tot_load)
        xx = range(len(rr))
        plt.plot(xx, rr, label='order param')
        plt.plot(xx, ss/max(ss), label='total load/{}'.format(max(ss)))
        
        plt.show()
        plt.title(str(self), fontsize=10)
        plt.xlabel('KM order parameter r')
        plt.ylabel('SP total load')
        plt.legend()
    
    def plot_cascade_measure_distr_for_r(self, rmin=0, rmax=1, error_split=1, 
                                log_bins=True, area_size=(False,True),
                                exo_seed_present=None, endo_seed_num=None,
                                min_tot_load=None, max_tot_load=None,
                                color=None):
        lbl = ' distribution'
        if exo_seed_present is not None:
            lbl += '; exo seed ' + \
                ('present' if exo_seed_present else 'absent')
        if endo_seed_num is not None:
            lbl += '; exactly {} endo seeds'.format(endo_seed_num)
        if min_tot_load is not None:
            lbl += '; S_min={}'.format(min_tot_load)
        if max_tot_load is not None:
            lbl += '; S_max={}'.format(max_tot_load)
        if rmin != 0:
            lbl += '; r_min={}'.format(rmin)
        if rmax != 1:
            lbl += '; r_max={}'.format(rmax)
        cc, ii = self.sp.get_cascades_with(exo_seed_present, endo_seed_num,
                                           min_tot_load, max_tot_load,
                                           with_indices=True)
        rr = np.array(self.km_r())[ii]
        
        cascs = [c for (c,r) in zip(cc[:], rr[:]) if rmin<r<rmax]

        
        area, size = area_size
        
        if area:
            out = self.sp._plot_log_histo_err([a for (a, s, t) in cascs], 
                          "cascade area", "probability",  
                          log_bins=log_bins, error_split=error_split,
                          label='area'+lbl, color=color)
        
        if size:
            out = self.sp._plot_log_histo_err([s for (a, s, t) in cascs], 
                          "cascade size", "probability",  
                          log_bins=log_bins, error_split=error_split,
                          label='size'+lbl, color=color)
        
        plt.title(str(self), fontsize=10)

        if area_size[0]^area_size[1]:
            return out
    
    @staticmethod
    def load_repair_save(name):
        ''' extend Savable load to also convert cascades from older runs (a,s) 
        to new format (a,s,t) '''
        kp = Saveable.load(name)
        if len(kp.sp.cascades[0]) == 2:
            kp.sp.cascades = [[a,s,0] for (a,s) in kp.sp.cascades]
            kp.save(name)
        
        