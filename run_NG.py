#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 15:56:50 2020

@author: guga
"""
from mod_NG import NetworkGenerator as NG
import matplotlib.pyplot as plt
import networkx as nx


n = 500
d = 3

ng = NG.random_regular(n, d)
# ng.add_random_loop(24,5)

ng.plot_expansion(normalize=False)



# ng.add_random_loop(5,5)
# ng.plot_ball_entropy_spectrum(cumulative=False)
# ng.plot_shell_entropy_spectrum(cumulative=False)
# ng.reconfigure().plot_ball_entropy_spectrum(cumulative=False)

# # net = nx.read_edgelist('Data/power_grid_net.txt')
# # ng = NG(net, 'US power grid')


# ngs = [#ng,
#        NG.random(n,e),
#        NG.random_regular(n,d),
#        NG.lattice_2d(20, 20, periodic=True),
#        NG.scale_free_static_model(n, 2., e=e),
#        NG.small_world(n,d,0.2),
#        NG.small_world(n,d,0.6)]
# # ng = NG.random(n,e)
# # ng = NG.random_regular(n,d)
# # ng = NG.lattice_2d(31, 32, periodic=True)
# # ng = NG.scale_free_static_model(n, 2., e=e)
# # for p in np.linspace(0,1,10):
# # ng = NG.small_world(n,d,0.1)
#     # ng.plot_entropy_spectrum()



# for ng in ngs:
#     print(ng)
#     # plt.figure(1)
#     # ng.plot_shell_entropy_spectrum(eliminate_collisions=False, cumulative=False)
#     # plt.pause(0.5)
#     # plt.figure(2)
#     ng.plot_ball_entropy_spectrum(eliminate_collisions=False, cumulative=True)
#     plt.legend()
#     plt.pause(0.5)

