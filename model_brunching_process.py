#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 23:32:41 2020

@author: guga
"""
from mod_KP import KuraPile
# from common import Saveable
from scipy.stats import binom
from scipy.special import poch
from scipy.special import betainc
from scipy.special import beta
from tqdm import tqdm
import matplotlib.pyplot as plt
import numpy as np
from numpy.polynomial.polynomial import Polynomial as poly
    

order_limit = 100

def plot(p, log_log=(True, True), label='', color=None):
    plt.plot(range(1,len(p.coef)), p.coef[1:], label=label, color=color)
    if log_log[0]:
        plt.xscale('log')
    if log_log[1]:
        plt.yscale('log')
    
def mean(p):
    ww = p.coef
    return sum([w*i for (i,w) in enumerate(ww)])/sum(ww)


n = 8000
d = 3
q = 0/n
c0 = 2

S_cutoff = 9800
S_min = 7400 #int((c0-1)*n*d/2/(d-1))
step_size = 1

r0 = 0.225
r1 = 0.624
r2 = 0.151


def p(S):
    S_new = S-S_min
    # return r2 + S_new/(10000-S_min)*(0.4-r2)
    return r2 + r1*S_new/n + r0*(S_new/n)**2
    # return (S-S_min)/n*S_min/n + (n-S_min)/n*((S-S_min)/n)**2


H = poly([0])
G_last = poly([1])

for S in tqdm(range(S_min, S_cutoff, step_size)):
    ps = p(S)
    
    x = poly([0, 1])
    F = poly([0])
    for i in range(order_limit):
        F = 1-ps*(1-q) + ps*(1-q)*x*F**(d-1)
        F = F.cutdeg(order_limit)
        
    # a_last = G_last.deriv()(1)
    # print(a_last*p(S))
    G = (1-ps + ps*x*F**d)*G_last(F)#*G_last(F)
    G = G.cutdeg(order_limit)
    # plot(G(poly([0,1])))
    # print(F(1))
    # print(G(1))
    
    H += G
    G_last = G

H /= H(1)
H /= 1-H.coef[0]


kploc = KuraPile.load("kp n=8000, p=20_n, #0")
kp = kploc

# kpgl = KuraPile.load("KP 8000 global, #0")

plot(H(poly([0,1])), label='branching process')
kploc.plot_cascade_measure_distr_for_r(rmin=0.9, rmax=1, error_split=5, min_tot_load=S_min, max_tot_load=S_cutoff)


# plt.figure()

    
# SS = np.linspace(S_min, S_cutoff, 100)
# pSpS = [p(S) for S in SS]
# plt.plot(SS, pSpS, color='orange')
kp.sp.plot_load_distribs_vs_S(j=None)
