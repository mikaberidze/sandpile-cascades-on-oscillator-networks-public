#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  9 17:10:20 2020

@author: guga
"""
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
from multiprocessing import Pool
from tqdm import tqdm
from common import Saveable
import copy

class WorkHorse(Saveable):
    
    def __init__(self, f=None, params=None, iters=None, parallel=True):
        ''' 
        WorkHorse will run function for you on provided parameters 
        iter-times for each value of param, calculate mean and std for each
        either sequentially (in which case it prints a progress bar) 
        or in parallel
        '''
        self.f = f
        self.pp = params
        self.iters = iters
        self.verbose = not parallel
        self.parallel = parallel
        self.max_processes = 32
        self.data = None
        self.f_out_num = 1
        
        self.name = ""
        self.xlabel = ""
        self.ylabels = None
        
        self.ignor_left = 0 #ignor first # data points
        self.ignore_last = None
        
        self.lable_font_size = 7
        self.first_in_its_kind = False
    
    def compute_data(self):
        ''' does the heavy lyfting: runs f on params, iter times for each '''
        if self.f_out_num==1:
            single_out = 0.
        else:
            single_out = [0.]*self.f_out_num
            
        self.data = np.array([[single_out]*self.iters]*len(self.pp))
        if self.parallel:
            pp_resh = np.empty([len(self.pp), self.iters], dtype=object)
            for i,p in enumerate(self.pp):
                for j in range(self.iters):
                    pp_resh[i,j] = (p, 
                                    j==0 and self.first_in_its_kind, #save
                                    i==0 and j==0 #verbose
                                    )
            pp_resh = pp_resh.reshape((self.iters*len(self.pp)))
            pool = Pool(min(self.iters*len(self.pp), self.max_processes))
            self.data = pool.starmap(self.f, pp_resh)
            pool.close()
            pool.join()
            self.data = np.array(self.data).reshape((len(self.pp), self.iters,\
                                                     self.f_out_num))
            
        else:
            self.pbar = tqdm(total=len(self.pp)*self.iters)
            for (i, p) in enumerate(self.pp):
                for j in range(self.iters):
                    self.data[i,j] = self.f(p)
                    self.pbar.update(1)
            
            self.pbar.close()
    
    def compute_means_and_errors(self):
        ''' from data it computes means and errors for each param value '''
        self.means = np.array(\
                                [np.mean(self.data[i], axis=0) \
                                 for i in range(len(self.pp))])
        self.errors = np.array(\
                                [stats.sem(self.data[i], axis=0) \
                                 for i in range(len(self.pp))])

    def plot_single(self, j, ax=plt, fmt='.'):
        ''' plots the means with error bars as a function of params '''
        xx = self.pp
        if not hasattr(self, 'ignor_left'): #obsolete
            self.ignor_left = 0
        if not hasattr(self, 'ignore_last'): #obsolete
            self.ignore_last = None
        st = self.ignor_left
        nd = self.ignore_last
        if nd is not None:
            nd = -nd
        if self.f_out_num==1:
            yy = self.means
            ee = self.errors
            ax.errorbar(xx[st:nd], yy[st:nd], yerr=ee[st:nd], fmt=fmt)
            if self.ylabels is not None:
                plt.ylabel(self.ylabels[0])
            if self.xlabel is not None:
                plt.xlabel(self.xlabel)
        else:
            yy = self.means[:,j]
            ee = self.errors[:,j]
            ax.errorbar(xx[st:nd], yy[st:nd], yerr=ee[st:nd], 
                        fmt=fmt, label=self.ylabels[j].replace('\n', ' '))
            if self.ylabels is not None:
                if ax!=plt:
                    plt.sca(ax)
                if not hasattr(self, 'lable_font_size'): #obsolete
                    self.lable_font_size = 7
                plt.ylabel(self.ylabels[j], fontsize=self.lable_font_size)
            if self.xlabel is not None:
                if ax!=plt:
                    plt.sca(ax)
                plt.xlabel(self.xlabel)
        
        ax.ticklabel_format(style='sci', axis='both', scilimits=(-2,2))
        plt.show()
        
        return xx, yy, ee
        
    def plot_as_subplots(self, lst=None):
        ''' plots different outputs in different subplots '''
        if lst is None:
            lst = range(self.f_out_num)
        
        fig = plt.gcf()
        axax = [fig.add_subplot(len(lst),1,1)]
        axax += [fig.add_subplot(len(lst),1,i+1, sharex=axax[0]) 
                 for i in range(1,len(lst))]
        # fig, axax = plt.subplots(len(lst), sharex=True, sharey=False, \
        #                         gridspec_kw={'hspace': 0})
        plt.xlabel(self.xlabel)
        # plt.suptitle(self.name, fontsize=10)
        for num, jj in enumerate(lst):
            if type(jj) is list:
                for j in jj:
                    self.plot_single(j, ax=axax[num])
                axax[num].legend(fontsize=self.lable_font_size)
                axax[num].set_ylabel('')
            else:
                self.plot_single(jj, ax=axax[num])
        
        # Hide x labels and tick labels for all but bottom plot.
        for ax in axax:
            ax.label_outer()
            
        self.axax = axax
        
    def plot(self, lst=None):
        ''' plots all f outputs as a single graph '''
        plt.suptitle(self.name, fontsize=10)
        if lst is None:
            lst = range(self.f_out_num)
        for j in lst:
            self.plot_single(j)
        if len(lst)>1:
            plt.legend()

    def work(self):
        ''' run the f; compute means and errors; plot results '''
        self.compute_data()
        self.compute_means_and_errors()
        if self.f_out_num == 1:
            self.plot()
        else:
            self.plot_as_subplots()
    
    @staticmethod
    def assamble(n, base="wh", save=False):  # obsolete
        whs = [0]*n
        for i in range(n):
            whs[i] = WorkHorse.load(base+str(i))
        wh = WorkHorse.merge(whs, verbose=True)
        
        if save:
            wh.save(base)
            
        return wh
    
    @staticmethod
    def merge(whs, verbose=False):
        ''' merge workhorses together '''
        data = {}
        
        mergers = whs
        if verbose:
            mergers = tqdm(whs)
        for wh in mergers:
            for (i, p) in enumerate(wh.pp):
                if p not in data:
                    data[p] = np.ndarray.tolist(wh.data[i])
                else:
                    data[p].extend(np.ndarray.tolist(wh.data[i])) 
        
        merged = WorkHorse()
        merged.f_out_num = whs[0].f_out_num
        merged.pp = np.array(sorted(data.keys()))
        merged.data = np.array([data[p] for p in merged.pp])
        
        merged.name = whs[0].name
        merged.xlabel = whs[0].xlabel
        merged.ylabels = whs[0].ylabels
        
        merged.compute_means_and_errors()
        
        return merged
    
    def __str__(self):
        return "WH " + self.name
    

    
    
    