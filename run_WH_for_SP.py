#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 12:53:25 2020

@author: guga
"""

from mod_KM import Kuramoto
from mod_KP import KuraPile
from mod_NG import NetworkGenerator
from mod_SP import Sandpile
from mod_WH import WorkHorse
import numpy as np
import sys


# Check if we are running on the cluster or on the local machine
if len(sys.argv)>1:
    running_on_cluster = True
    job = int(sys.argv[1])-1 
else: 
    running_on_cluster = False

n = 10
d = 3
c = 2

dissipation_p = 20/n

t_run = 10*n*c

params = np.linspace(0, 1, num=32)# np.logspace(-4,-1,16)# list(range(3,9))# #TODO!
iters = 16

def f(param, save=False, verbose=False):
    ng = NetworkGenerator.random_regular(n, d)
    
    sp = Sandpile(ng)
    sp.set_all_node_caps(c)
    sp.dissipation_p = dissipation_p
    sp.control_p = param
    
    # print("relaxing")
    sp.relax()
    
    sp.drop_and_casc(n=t_run)
    
    if save:
        sp.save(str(sp))
    area, size, time = sp.get_avg_measures()
    load_liminf, load_limsup = sp.get_stable_load_range()
    sp_freq, sp_ampl = sp.typical_frequency()
    
    return (sp.avg_number_of_grains(),
            area, size, time,
            sp.get_black_swan_probability()[0],
            sp.get_black_swan_probability()[1]
            )


wh = WorkHorse(f=f, params=params, iters=iters, parallel=running_on_cluster)
wh.f_out_num = 6
wh.first_in_its_kind = (not running_on_cluster) or job==0
wh.compute_data()


wh.name = f'WH of SP: d={d} n={n}, p={dissipation_p}, control_p=var'


wh.xlabel = 'control p'
wh.ylabels = ["Average load",
              "avg. area", "avg. size", "avg. lifetime",
              "Black \nSwan \nprob. \n(area>50%)",
              "Black \nSwan \nprob. \n(size>50%)",
              ]


if running_on_cluster:
    wh.save(f'{wh} job#{job}')
    print(f'\n{wh} job#{job}')
else:
    wh.compute_means_and_errors()
    wh.plot_as_subplots()



