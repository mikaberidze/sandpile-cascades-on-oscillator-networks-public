M = csvread('./Out/sp_state.txt');

M = M';
[U,S,V] = svd(M, 'econ');
diag(S)
plot(diag(S),'.','MarkerSize',30)
plot(-U(:,1), '.','MarkerSize',10)
ylim([-.2 .2]);
xlim([20500,22600])
plot(-V(:,1), 'LineWidth',1,'Color','r')