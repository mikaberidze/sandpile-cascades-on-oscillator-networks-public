#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 23:32:41 2020

@author: guga
"""
from mod_KP import KuraPile
# from common import Saveable
from scipy.stats import binom
from scipy.special import poch
from scipy.special import betainc
from scipy.special import beta
from tqdm import tqdm
import matplotlib.pyplot as plt
import numpy as np
from numpy.polynomial.polynomial import Polynomial as poly
    

order_limit = 100

def plot(p, log_log=(True, True), label='', color=None):
    plt.plot(range(1,len(p.coef)), p.coef[1:], label=label, color=color)
    if log_log[0]:
        plt.xscale('log')
    if log_log[1]:
        plt.yscale('log')
    
def mean(p):
    ww = p.coef
    return sum([w*i for (i,w) in enumerate(ww)])/sum(ww)


# kp = KuraPile.load('3-regular, n=1600; KP dω_ds=0.25, dc_dr=1.1, ΔT=1; KM k=0.333, <r>=0.898, t_run=15000.0; SP dissip=0.0125, <s>_node=2.006')

n = 8000
d = 3
q = 20/n
c0 = 2

S_cutoff = 9000
S_min = int((c0-1)*n*d/2/(d-1))
step_size = 50

# def analytic(s):
#     Sf = int((c0-1)*n*d/2/(d-1))
#     return 4**(s-1)*s*n/(S_cutoff-Sf)*poch(3/2, s-1)/poch(4, s-1)*betainc(1+s,3+s,(S_cutoff-Sf)/n)*beta(1+s,3+s)

# def analytic2(s):
#     Sf = int((c0-1)*n*d/2/(d-1))
#     return 4**(s-1)*s*n/(S_cutoff-S_min)*poch(3/2, s-1)/poch(4, s-1)*\
#             (betainc(1+s,3+s,(S_cutoff-Sf)/n)*beta(1+s,3+s)-
#              betainc(1+s,3+s,(S_min-Sf)/n)*beta(1+s,3+s))


H = poly([0])
norm = 0
for S in tqdm(range(S_min, S_cutoff, step_size)):
    def p(S):
        return (S-S_min)/n*S_min/n + (n-S_min)/n*(S-S_min)**2/n**2#
        # for MFT: 
        # for c0=2: 0.03 + 0.5*S/n + 0.35*S**2/(2*n**2) + 0.15*S**3/(6*n**3) 
        # for c0=2: 0.1 + 0.65*S/n + 0.25*S**2/(2*n**2)
    def dSdt(S):
        pS = p(S)
        aS = (pS+pS**2)/(1-2*pS)
        return 1# - q*aS
    
    ps = p(S)
    
    x = poly([0, 1])
    F = poly([0])
    for i in range(order_limit):
        F = 1-ps+q + (ps-q)*x*F**(d-1)
        F = F.cutdeg(order_limit)
    
    G = 1-ps + ps*x*F**d
    G = G.cutdeg(order_limit)
    # plot(G(poly([0,1])))
    
    norm += 1/dSdt(S)
    H += G/dSdt(S)

H /= norm
H /= 1-H.coef[0]


plot(H(poly([0,1])), label='branching process')

# xx = [i+1 for i in range(order_limit)]
# yy1 = [analytic(x) for x in xx]
# yy2 = [analytic2(x) for x in xx]
# plt.plot(xx, yy1)
# plt.plot(xx, yy2)


kp8k1.plot_cascade_measure_distr_for_r(rmin=0.9, rmax=1, error_split=5,
                                       min_tot_load=S_min, max_tot_load=S_cutoff)
kp8k2.plot_cascade_measure_distr_for_r(rmin=0.9, rmax=1, error_split=1,
                                       min_tot_load=S_min, max_tot_load=S_cutoff)

# kp8k1.draw_r_vs_tot_load()
