                                #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 12:53:25 2020

@author: guga
"""

from mod_KP import KuraPile
from mod_SP import Sandpile
from mod_KM import Kuramoto
from mod_NG import NetworkGenerator
from mod_PLP import PhaseLoadPlot
import matplotlib.pyplot as plt
import numpy as np
import sys

# Check if we are running on the cluster or on the local machine
if len(sys.argv)>1:
    running_on_cluster = True
    job = int(sys.argv[1])-1
else:
    running_on_cluster = False

params = [1000,2000,3000,4000,5000,6000,7000,8000,9000,10000]
if running_on_cluster:
    param = params[job%len(params)]
    first_in_its_kind = job//len(params)==0
else:
    param = params[0]

# ----------------------------------------------------- All the parameters
n = 500
d = 3
verbose = (not running_on_cluster) or first_in_its_kind

c = 2
dissipation_p = 0
k = 1

loops = 1
smin = 300
smax = 800

dc_dr = 1
dT = 1
kp_toppler = True # if it should use randomization phase as backward coupling
punishing = True
sp_coupling = 'global'# 'discrete'# 'local'#
local_order_param = 'km'# 'our'#
dw_ds = 0.
w0 = 0

reinforce_rule = None# 'reinforce_deepen_a_node'# 'reinforce_deepen_10_nodes'#

km_relax = 50
kp_relax = 200

kp_collecting_seed_stats = False
phase_monitor = False
animate = False
show_cascade_evolution = True
fancy_animation = True
save_fancy_anim = False

record_state_matrix_SP = False #TODO!

base_name = 'KP 8000 global'

# ----------------------------------------------------- End of params

print("Generating a network", flush=True)
ng = NetworkGenerator.random_regular(n, d)

print("initializing a Sandpile object", flush=True)
sp = Sandpile(ng)
sp.set_all_node_caps(c)
sp.additional_info = 'c₀=' + str(c)
sp.dissipation_p = dissipation_p
sp.record_state_matrix = record_state_matrix_SP
sp.add_grains_randomly(smin)

print("initializing a Kuramoto object", flush=True)
km = Kuramoto(ng, k=k)
km.phase_monitor = phase_monitor
km.w_normal_distr(w0, 0)
km.init_phase_uniform_distr(width=np.pi)

print("initializing KuraPile object", flush=True)
kp = KuraPile(km, sp, ng)
kp.verbose = verbose
kp.show_cascade_evolution = show_cascade_evolution
if reinforce_rule == 'reinforce_deepen_10_nodes':
    kp.reinforce_rule = kp.reinforce_deepen_10_nodes
elif reinforce_rule == 'reinforce_deepen_a_node':
    kp.reinforce_rule = kp.reinforce_deepen_a_node

if animate and fancy_animation:
    print("force-balance the network", flush=True)
    t = 1
    plp = PhaseLoadPlot(ng.net)
    plp.include_order_param(0)
    plp.include_order_param(0)
    plp.interactive_force_layout()
    plp.save_animation = save_fancy_anim
    sp.expected_load_bounds = [(c-dc_dr)*n*d/2/(d-1), c*n*d/2/(d-1)]

kp.dw_ds = dw_ds
kp.dcap_dr = dc_dr
kp.sandfall_period = dT
kp.collecting_seed_stats = kp_collecting_seed_stats
kp.punishing = punishing
if animate and fancy_animation:
    kp.plp = plp
if kp_toppler: 
    kp.km_update_rule = kp.randomize_km_phases_for_toppled
else:
    kp.km_update_rule = lambda: None
if sp_coupling=='discrete':
    kp.sp_update_rule = kp.set_sp_caps_discretely
elif sp_coupling=='global':
    kp.sp_update_rule = kp.set_sp_caps_by_global_r
else:
    if local_order_param == 'km':
        km.get_local_order_param = km.calc_local_km_order_param
    
print("\nKP info: ", kp, flush=True)

km.relax(km_relax, verbose=verbose)

if verbose: print("\ncollecting cascade data", flush=True)
# kp.animate = True
ss = []
act = []
for i in range(loops):
    ss1, act1 = kp.run_hysteresis(ascending=True, n=smax-smin)
    ss2, act2 = kp.run_hysteresis(ascending=False, n=smax-smin)
    ss.extend(ss1)
    ss.extend(ss2)
    act.extend(act1)
    act.extend(act2)


name = str(kp)

# save if on cluster
if running_on_cluster:
    # # save SP
    # sp.kp = None
    # sp.name = name
    # sp.save(base_name + "_" + str(job//len(params)))
    
    # if first_in_its_kind and (param==params[0] or param==params[-1]):
    #     kp.save("kp stability check - " + base_name)
    
    # save KP
    kp.save(base_name + ", #" + str(job//len(params)))

else:
    plt.plot(ss,act)
    if animate and fancy_animation and save_fancy_anim:
        plp.export_animation()
    pass

