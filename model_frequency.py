#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 21:42:26 2020

@author: guga
"""
from mod_KP import KuraPile
from mod_SP import Sandpile
from mod_WH import WorkHorse
from tqdm import tqdm
import matplotlib.pyplot as plt
import numpy as np
import scipy
from common import *


def theo_range(n, d, dcdr, c0):
    return (n*d*(c0+dcdr/2-0.5)/2/(d-1), n*d*int(c0+dcdr)/2/(d-1)) 

def plot_sand_range(n, d, dcdr_min, dcdr_max, c0):
    xx = np.linspace(dcdr_min, dcdr_max, num=128)
    ww = [theo_range(n, d, dcdr, c0) for dcdr in xx]
    plt.plot(xx, ww)

def w_theo(n, d, dcdr, dT=1, fit_param=1):
    del_r = np.floor(dcdr*0.1)+np.ceil(0.9*dcdr)# int(dcdr)-(dcdr/2)+0.5
    if dcdr*0.1==int(dcdr*0.1) or dcdr*0.9==int(dcdr*0.9):
        return None
    return 2*(d-1)/n/d/dT/del_r

def plot_w_vs_dcdr_theo(n, d, dcdr_min, dcdr_max, dT=1, fit_param=0):
    dx = 0.01/0.9
    xx = [dcdr_min+dx*i for i in range(int((dcdr_max-dcdr_min)/dx))]
    ww = [w_theo(n, d, dcdr, dT, fit_param) for dcdr in xx]
    plt.plot(xx, ww, label='Analytical model')
    plt.xlabel('dc/dr')
    plt.ylabel('Typical frequency ω (1/sec)')
    # plt.ylim(0, 1.2*np.nanmax(np.array(ww, dtype=np.float64)))
    plt.xlim(0, 1.2*max(xx))
    
def S_i(n,d,c0,dcdr,l):
    return n*np.floor(c0 - dcdr*(l-1))*d/2/(d-1)
def S_f(n,d,c0,dcdr,l):
    return n*np.floor(c0 - dcdr*l)*d/2/(d-1)

def plot_fluctuation_range(n,d,c0,dcdrr,l):
    ssi = [S_i(n,d,c0,dcdr,l) for dcdr in dcdrr]
    ssf = [S_f(n,d,c0,dcdr,l) for dcdr in dcdrr]
    plt.plot(dcdrr, ssi, label='limsup S')
    plt.plot(dcdrr, ssf, label='liminf S')
    
def plot_w_vs_d_theo(n, dcdr, d_min, d_max, dT=1):
    dd = np.linspace(d_min, d_max, num=128)
    ww = [w_theo(n, d, dcdr, dT) for d in dd]
    plt.plot(dd, ww)
    plt.xlabel('d')
    plt.ylabel('Typical frequency ω (rad/sec)')
    plt.ylim(0, 1.3*max(ww))
    plt.xlim(0, 1.1*max(dd))
    # plt.ylim(0, 1.2*max(ww))
    # plt.xlim(0, 1.2*max(xx))

def fit_freq_vs_dcdr(xx, yy, ignore=[]):
    def func(xx, param, par2):
        return [w_theo(n, d, x, dT=1, fit_param=param) for x in xx]
    xx = np.delete(xx,ignore)
    yy = np.delete(yy,ignore)
    popt, pcov = scipy.optimize.curve_fit(func, xx, yy)
    

    # plt.plot(x, func(xx,a), '--', linewidth=1, label='x^({:.3f})'.format(a))
    return popt[0]
    


n = 8000
d = 3
dcdr = 1.1
c0 = 5
l = 0.9

# wh_names = ['WH 3-regular, n=1600; KP topple->rnd φ, local dc_dr=var, ΔT=1; KM k=3.0, ω=0; t_run=20000; SP dissip=0.01, c₀=2']

# plot_fluctuation_range(n,d,c0,np.linspace(0.1, 5.3, 100),l)


# for whn in wh_names:
#     wh = WorkHorse.load(whn)
#     xx, yy, ee = wh.plot_single(3)
    
#     # fit_par = fit_freq_vs_dcdr(xx, yy, ignore=[4,5,6,7])
    
#     # print(wh, 'fitted_params=',fit_par)
#     plot_w_vs_dcdr_theo(n, d, 0.5, 5.4)
#     # plot_w_vs_d_theo(n, dcdr, 2, 9)
    
#     # plt.figure()
#     # wh.plot([7,8])
#     wh.plot_as_subplots([0,1,2,[3,4],[5,6],[7,8]])
#     # plot_sand_range(n, d, 1, 9, c0)
    
    
    
# plt.title(wh, fontsize=10)


