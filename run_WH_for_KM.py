#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 12:53:25 2020

@author: guga
"""

from mod_KM import Kuramoto
from mod_KP import KuraPile
from mod_NG import NetworkGenerator
from mod_SP import Sandpile
from mod_WH import WorkHorse
import networkx as nx
import numpy as np
import sys


# Check if we are running on the cluster or on the local machine
if len(sys.argv)>1:
    running_on_cluster = True
    job = int(sys.argv[1])-1 
else: 
    running_on_cluster = False

n = 4000
d = 3
k = 1
del_w = 1
w0 = 0

t_run = 100

params = np.linspace(0, 5, num=16)# np.logspace(-4,-1,16)# list(range(3,9))# #TODO!
iters = 5

def f(k, save=False, verbose=False):
    ng = NetworkGenerator.random_regular(n, d)
    
    km = Kuramoto(ng, k)
    
    km.w_normal_distr(w0, del_w)
    km.init_phase_uniform_distr()
    km.phase_monitor = True
    km.run(t_run)
    km.plot_r_vs_t()
    
    return (km.get_avg_order_param(t_run/3))


wh = WorkHorse(f=f, params=params, iters=iters, parallel=running_on_cluster)
wh.f_out_num = 1
wh.first_in_its_kind = (not running_on_cluster) or job==0
wh.compute_data()


wh.name = f'WH of KM: d={d} n={n}, k={k}, Δω={del_w}'


wh.xlabel = '$k$'
wh.ylabels = ["$r$"]


if running_on_cluster:
    wh.save(f'{wh} job#{job}')
    print(f'\n{wh} job#{job}')
else:
    wh.compute_means_and_errors()
    wh.plot_as_subplots()

