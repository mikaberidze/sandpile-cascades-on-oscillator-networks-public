#!/bin/sh
#SBATCH --job-name=KP_sweep_alpha
#SBATCH --mail-type=END,FAIL        # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=mikaberidze@ucdavis.edu     # Where to send mail

#SBATCH --output=Out/KP_sweep_alpha_%A-%a.log
#SBATCH --partition=parallel
##SBATCH --time=5-00:00:00

#SBATCH --cpus-per-task=32
##SBATCH --mem-per-cpu=8000

#SBATCH --array=1-15         # Array range


python3 run_WH.py $SLURM_ARRAY_TASK_ID

