#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 16 22:43:32 2020

@author: guga
"""

from common import Saveable
import matplotlib.pyplot as plt
import sys
import numpy as np
import scipy
import random
from scipy.integrate import solve_ivp
from scipy.interpolate import interp1d
from tqdm import tqdm


class Kuramoto(Saveable):
    def __init__(self, ng, k=1, kp=None):
        '''
        itializer

        Parameters
        ----------
        ng : NetworkGenerator
            network on which kuramoto ans sandpile run.
        k : float, optional
            coupling of oscillators. The default is 1.
        '''
        self.ng = ng
        self.n = ng.net.number_of_nodes()
        self.net = ng.net
        self.k = k
        self.kp = kp

        # initialize variables that will be specified later
        self.t = None
        self.phi_discr = [np.empty(0)]
        self.r_discr = np.empty(0)
        self.phi_instant = np.empty(self.n)
        # could be used to correct animation colors
        # self.k_history = np.empty(0) 
        self.phase_monitor = False
        self.data_monitor = True
        self.w = None
        self._verbose_RK = False
        self.w_info = None
        self.phi0_info = None
        
        self.get_local_order_param = self.calc_local_km_order_param

        self.anim_frames = None
        self.ax = None
        self.fig = None
        plt.ion()
        
    def set_w(self, nd, w):
        ''' set node natural frequency '''
        self.w[nd] = w
        self.w_info = None

    def get_w(self, nd):
        ''' get node natural frequency(ies) '''
        if type(nd) is int:
            return self.w[nd]
        if type(nd) is list:
            return [self.w[n] for n in nd]
        raise Exception("received the node of type "+str(type(nd)))

    def set_phase(self, nd, phi):
        ''' Set node phase '''
        self.phi_instant[nd] = phi

    def get_phase(self, nd):
        ''' get node phase(s) '''
        if type(nd) is int:
            return self.phi_instant[nd]
        if type(nd) is list:
            return [self.phi_instant[nd] for n in nd]

    def w_normal_distr(self, w0, std=0):
        '''
        sample oscillator frequencies from normal distribution

        Parameters
        ----------
        w0 : float
            mean value of distribution.
        std : float, optional
            standard deviation. The default is 0.
        '''
        if std==0:
            self.w_info = ', ω=' + str(w0)
        else:
            self.w_info = ', ω=normal(' + str(w0) + ', ' + str(std) + ')'
        self.w = {nd: np.random.normal(w0, std) for nd in self.net.nodes()}

    def w_uniform_distr(self, w0, w1):
        '''
        sample oscillator frequencies from uniform distribution

        Parameters
        ----------
        w0 : float
            lower bound.
        w1 : float
            upper bound.
        '''
        self.w_info = ', ω=uniform(' + str(w0) + ', ' + str(w1) + ')'
        self.w = {nd: np.random.uniform(w0, w1) for nd in self.net.nodes()}

    def init_phase_normal_distr(self, mean, std=0):
        '''
        sample initial phases of oscillators from a normal distributed
        '''
        self.phi_instant = np.random.normal(mean, std, self.n)
        self.t = [0]
        self.r_discr = [self.calc_order_param()]
        if self.phase_monitor:
            self.phi_discr = [self.phi_instant]

    def init_phase_uniform_distr(self, width=2*np.pi):
        '''
        sample initial phases of oscillators from the uniform distributed
        '''
        self.phi_instant = np.random.uniform(0, width, self.n)
        self.t = [0]
        self.r_discr = [self.calc_order_param()]
        if self.phase_monitor:
            self.phi_discr = [self.phi_instant]

    def circularly_wrap_init_phases(self, std=0, winding_number=1):
        '''
        set initial phases of oscillators close to different topologies of
        solutions for ring network.

        Parameters
        ----------
        std : float, optional
            deviation of each initial phase from the deterministic value.
            The default is 0.
        winding_number : int, optional
            self explanatory. The default is 1.
        '''
        self.phi0_info = ', φ uniform w/ ' + str(winding_number) + ' loops '\
            + '±' + str(std)

        self.phi_instant = np.linspace(0, winding_number*2*np.pi,
                                      self.n+1)[0:-1]
        self.phi_instant += 0.1 + np.random.normal(0, std, self.n)
        self.t = [0]
        self.r_discr = [self.calc_order_param()]
        if self.phase_monitor:
            self.phi_discr = [self.phi_instant]

    def flip_time(self):
        ''' After invoking this method,the time evolution of the system
        will run backwards '''
        # notice that flipping sign of t in the evolution equation is
        # equivalent to flipping sign of k and all w-s

        self.k = -self.k
        for nd in self.oscillators():
            self.set_w(nd, -self.w[nd])

    @staticmethod
    def der(t, phi, kura, tmin, tmax):
        '''
        compute speed of phase point

        Parameters
        ----------
        t : float
            current time (no use).
        phi : float
            given phase point.
        kura : TYPE
            kuramoto object.

        Returns
        -------
        der_phi : n dimensional vector.
        '''

        if kura._verbose_RK:  # show the progress
            prog = (t-tmin)/(tmax-tmin)
            if not hasattr(kura, "last_progress") or \
                    kura.last_progress + 0.01 < prog:

                sys.stdout.write("\r Runge-Kutta progress: " +
                                 str(prog+10**(-5))[0:4])
                sys.stdout.flush()
                kura.last_progress = prog
            if prog >= 0.999:
                kura.last_progress = 0

        der_phi = [0]*kura.net.number_of_nodes()

        for i in range(kura.net.number_of_nodes()):
            der_phi[i] = kura.w[i]  # assumming nodes are numbered from 0 to n

        for nd in kura.oscillators():
            for neighbour in kura.net.neighbors(nd):
                der_phi[nd] += np.sin(phi[neighbour]-phi[nd])*kura.k

        return der_phi

    def _phi_for_single_t(self, t):
        '''
        return the phase point time t
        '''
        if t < self.t[0] or t > self.t_final():
            raise Exception("t={} is outside simulated range ({},{})".\
                            format(t,self.t[0], self.t_final()))
        if t == self.t_final():
            return self.phi_instant
        # find last element in self.t that <= t
        i_lo = 0
        i_up = len(self.t)-1

        # check endpoints
        if t == self.t[i_lo]:
            return self.phi_discr[i_lo]
        if t == self.t[i_up]:
            return self.phi_discr[i_up]

        # binary search otherwise
        i = 0
        while i_up - i_lo != 1:
            i = int((i_up + i_lo) / 2)
            if self.t[i] == t:
                return self.phi_discr[i]
            if self.t[i] < t:
                i_lo = i
            if self.t[i] > t:
                i_up = i

        if t < self.t[i_lo] or t > self.t[i_up] or i_up-i_lo != 1:
            raise Exception("something went wrong here")

        # get corresponding weights to linearly interpolate
        weight_lo = (self.t[i_up] - t) / (self.t[i_up] - self.t[i_lo])
        weight_up = (t - self.t[i_lo]) / (self.t[i_up] - self.t[i_lo])

        return weight_lo*self.phi_discr[i_lo] + weight_up*self.phi_discr[i_up]

    def phi(self, t):
        '''
        return the phase point coordinates at times t
        '''
        # if t==self.t_final():
        #     return self.phi_instant
        if type(t)==float or type(t)==int or type(t)==np.float64 or\
                type(t)==np.int64:
            return self._phi_for_single_t(t)

        return [self._phi_for_single_t(tt) for tt in t]

    def run(self, del_t, verbose=False):
        '''
        runs the given kuramoto model for next del_t seconds

        Parameters
        ----------
        del_t : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        '''
        
        t_start = self.t_final()
        t_end = self.t_final() + del_t
        self._verbose_RK = verbose
        
        sol = solve_ivp(lambda t, phi: self.der(t, phi, self, t_start, t_end),
                        [t_start, t_end], self.phi_instant)
        if self.data_monitor:
            self.t.extend(sol.t[1:])
            for phi_arr in np.transpose(sol.y)[1:]:
                
                self.r_discr.append(self.calc_order_param(phis=phi_arr))
                if self.phase_monitor:
                    self.phi_discr.append(phi_arr)
        self.phi_instant = np.transpose(sol.y)[-1]
            
        if verbose: print("")

    def relax(self, time, verbose=False):
        ''' run KM without recording anything '''
        if verbose:
            print("relaxing KM")
        was_watching = self.data_monitor
        self.data_monitor = False
        self.run(time, verbose=verbose)
        self.data_monitor = was_watching
        self.reset_discrete_phases()
        if verbose:
            print("relaxation of KM finished")
    
    def reset_discrete_phases(self):
        self.phi_discr = [self.phi_instant]
    
    def __str__(self):
        name = "KM k=%.3f" % self.k
        if self.w_info is not None:
            name += self.w_info
        if self.phi0_info is not None:
            name += self.phi0_info
        name += ", <r>=%.3f" % self.get_avg_order_param(self.t_final()*0.5)
        name += ", t_run=" + str(self.t_final())
        return name            
        

#------------------------------------------------------------------------- Analysis
    def calc_phase_center(self, phis):
        '''
        get the x, y coordinates of phase center (order parameter) at time t
        '''
        xx = np.cos(phis)
        yy = np.sin(phis)

        return (np.mean(xx), np.mean(yy))

    def calc_order_param(self, phis=None, t=None):
        if phis is None:
            if t is None:
                phis = self.phi(self.t_final())
            else:
                phis = self.phi(t)

        return np.linalg.norm(self.calc_phase_center(phis))
    
    def get_order_param(self, t=None):
        if self.t_final() == t or t is None:
            return self.r_discr[-1]
        if not hasattr(self, 'r_smooth') or self.r_smooth.t_max<self.t_final():
            # print(f"t discr: {self.t},  and t inquired: {t}")
            self.r_smooth = interp1d(self.t, self.r_discr, kind='linear')
            self.r_smooth.t_max = self.t_final()
            
        return self.r_smooth(t)
    
    def calc_avg_phase(self, t=None):
        if t is None:
            t = self.t_final()
        
        phis = self.phi(t)
        return np.arctan2(self.calc_phase_center(phis)[1],
                          self.calc_phase_center(phis)[0])

    def calc_local_km_order_param(self, node, t=None):
        ''' KM r of the neighborhood of node '''
        if t is None:
            t = self.t_final()

        x_avg = np.cos(self.phi(t)[node])
        y_avg = np.sin(self.phi(t)[node])
        neighbors = [nd for nd in self.net.neighbors(node)]
        for nbr in neighbors:
            x_avg += np.cos(self.phi(t)[nbr])
            y_avg += np.sin(self.phi(t)[nbr])

        x_avg /= (len(neighbors)+1)
        y_avg /= (len(neighbors)+1)

        return np.linalg.norm([x_avg, y_avg])

    def calc_local_order_param_our(self, node, t=None):
        ''' more precise local order treating the central node 
        with distinction '''
        if t is None:
            t = self.t_final()
        x_avg = 0
        # y_avg = 0
        neighbors = [nd for nd in self.net.neighbors(node)]
        for nbr in neighbors:
            x_avg += np.cos(self.phi(t)[nbr]-self.phi(t)[node])
            # y_avg += np.sin(self.phi(t)[nbr]-self.phi(t)[node])

        x_avg /= len(neighbors)
        # y_avg /= len(neighbors)

        return (x_avg+1)/2

    def get_del_phi_max(self, node, t=None):
        ''' get the phase difference with least synchronized neighbor '''
        if t is None:
            t = self.t_final()

        del_phi_max = 0
        # y_avg = 0
        neighbors = [nd for nd in self.net.neighbors(node)]
        for nbr in neighbors:
            del_phi = abs(self.phi(t)[nbr]-self.phi(t)[node])%(2*np.pi)
            if del_phi>del_phi_max:
                del_phi_max = del_phi

        return del_phi_max
    
    def get_local_order_param_avg(self, t=None, sample_size=100):
        ''' return the average of local order parameter over the nodes 
        at the given time instant, or at the final time if t=None'''
        sum_r_loc = 0
        if sample_size is None:
            sample_size = self.n
        for nd in random.sample(self.oscillators(), sample_size):
            sum_r_loc += self.calc_local_order_param(nd, t=t)
        return sum_r_loc / sample_size
    
    def get_neighbors_avg_phase(self, node, t=None):
        if t is None:
            t = self.t_final()

        x_avg = 0
        y_avg = 0
        neighbors = [nd for nd in self.net.neighbors(node)]
        for nbr in neighbors:
            x_avg += np.cos(self.phi(t)[nbr])
            y_avg += np.sin(self.phi(t)[nbr])

        x_avg /= len(neighbors)
        y_avg /= len(neighbors)
        
        return np.arctan2(y_avg, x_avg)

    def get_avg_order_param(self, last_seconds):
        '''
        get average order parameter (just magnitude) from the given last
        seconds of the simulation
        '''
        r_subset = [self.r_discr[i] for i in range(len(self.t)) \
                    if self.t[i]>self.t_final()-last_seconds]
        if len(r_subset)==0:
            return np.nan
        return np.mean(r_subset)

    def get_min_order_param(self, last_seconds):
        '''
        get minimal value of order parameter (just magnitude) from the last 
        given seconds of the simulation
        '''
        r_subset = [self.r_discr[i] for i in range(len(self.t)) \
                    if self.t[i]>self.t_final()-last_seconds]
        return np.min(r_subset)

    def t_final(self):
        ''' the final time to which km has been simulated so far '''
        return self.t[-1]

    def oscillators(self):
        ''' return list of nodes in the network '''
        return self.net.nodes()
    
    def order_param_FT(self, points=None):
        ''' fourier transform of order parameter '''
        # Number of samplepoints
        if points is None:
            points = len(self.t)
        tt = np.linspace(0, self.t_final(), points)
        data = self.r_discr
        data = np.array(data) - np.mean(data)
        N = len(data)

        freq = np.linspace(0.0, 1.0/(2.0), N//2) / (tt[1]-tt[0])
        ampl = scipy.fftpack.fft(data)
        ampl = 2.0/N * np.abs(ampl[:N//2])

        return freq, ampl
    
    def typical_frequency(self):
        ''' typical frequency of large cascades (1/sec) '''
        freq, ampl = self.order_param_FT()
        maxAmpl = 0
        typical_freq = 0

        for i in range(len(freq)):
            if ampl[i] > maxAmpl:
                maxAmpl = ampl[i]
                typical_freq = freq[i]

        return typical_freq, maxAmpl

#------------------------------------------------------------------------- Plotters
    def plot_phases_vs_t(self, points=1000):
        '''
        plot phases of each oscillator vs time
        '''
        t = np.linspace(0, self.t_final(), points)
        z = self.phi(t)
        z = np.mod(z, 2*np.pi)

        for i in range(1, len(z)):
            for j in range(self.n):
                if abs(z[i, j] - z[i-1, j]) > 1:
                    z[i-1, j] = np.nan

        plt.plot(t, z)
        plt.xlabel('t')
        plt.title('phases vs time')
        plt.ylim(0, 2*np.pi)
        plt.show()
        
    def plot_relative_phases_vs_t(self, nodes, origin=None, points=1000):
        '''
        plot relative phases of specified oscillator wrt the node 'origin' or
        wrt the average phase if 'origin' is None vs time
        '''
        tt = np.linspace(0, self.t_final(), points)
        all_phi = np.array(self.phi(tt)).T
        if origin is None:
            mean = [self.calc_avg_phase(t) for t in tt]
            z = all_phi[nodes] - np.array(mean)
        else:
            z = all_phi[nodes] - all_phi[origin]
        z = np.mod(z.T+np.pi, 2*np.pi) - np.pi

        for i in range(1, len(z)):
            for j in range(len(z[0])):
                if abs(z[i, j] - z[i-1, j]) > 1:
                    z[i-1, j] = np.nan

        plt.plot(tt, z)
        plt.xlabel('t')
        plt.ylabel('relative phases')
        
        plt.title('relative phases vs time')
        plt.grid(True)
        plt.ylim(-np.pi, np.pi)
        plt.show()

    def plot_r_vs_t(self, use_r_discr=True):
        ''' plot order parameter vs time '''
        if use_r_discr:
            tt = self.t
            rr = self.r_discr
        else:
            points=1000
            tt = np.linspace(0, self.t_final(), points)
            rr = [self.calc_order_param(t) for t in tt]
        # plt.figure()
        plt.plot(tt, rr, label='KM order param.')
        plt.xlabel('t')
        plt.ylabel('r (order param)')
        plt.title(str(self), fontsize=10)
        plt.ylim(0, 1.1)
        plt.show()
        
    def plot_r_FT(self, points=10000, scaled=True):
        ff, aa = self.order_param_FT(points=points)
        if scaled:
            aa /= max(aa)
        plt.plot(ff, aa)
        plt.xlabel('frequency (1/sec)')
        plt.ylabel('amplitude')
        plt.title(str(self), fontsize=10)
        plt.show()

    def plot_r_loc_avg_vs_t(self, points=1000):
        ''' plot order parameter vs time '''
        tt = np.linspace(0, self.t_final(), points)
        rr = [self.get_local_order_param_avg(t, sample_size=100) for t in tt]
        # plt.figure()
        plt.plot(tt, rr, label='KM avg loc. order param.')
        plt.xlabel('t')
        plt.ylabel('<r_loc> (avg. local order param)')
        plt.title(str(self), fontsize=10)
        plt.ylim(0, 1.1)
        plt.show()

    def set_plot_ax(self, ax, fig):
        self.ax = ax
        self.fig = fig
        ax.axis('off')

    def get_plot_ax(self):
        if self.ax is None:
            self.fig, self.ax = plt.subplots()
        elif not plt.fignum_exists(self.fig.number):
            self.fig, self.ax = plt.subplots()
        
        return self.ax

    def animate(self, last=None, first=None, local_colors=False, n=20):
        '''
        animates the phase circle
        '''
        tmin = 1
        tmax = self.t_final()
        if last is not None:
            tmin = tmax - last
        if first is not None:
            tmax = first

        for t in np.linspace(tmin, tmax, n):
            self.draw_phase_circle(t, local_colors=local_colors)

    def draw_phase_circle(self, t=None, fig=1, local_colors=False):
        '''
        show the phase of each oscillator on a circle, at given time moment t
        '''
        if t is None:
            t = self.t_final()
        phis = self.phi(t)  # get the phases
        
        if hasattr(self, "plp"):
            self.plp.phases = phis
            self.plp.title = f't={np.round(t, 1)}'
            self.plp.include_order_param(self.get_order_param(t))
            self.plp.plot_2d()
            #TODO!
            return

        # choose the delta r to offset pts
        delta_r = 0.5*(1 - np.e**(-self.n/50))

        # generate data point for each oscillator
        if self.kp is None or local_colors:
            colors = self.node_colors_by_frequency()
        else:
            colors = np.array([self.kp.sp.color_of_node(nd)
                               for nd in range(self.n)])

        xx = [0]*self.n
        yy = [0]*self.n

        
        for (i, nd) in enumerate(self.net.nodes()):
            r = 0.98 + delta_r*np.sin(2*np.pi*i/self.n)
            xx[nd] = r*np.cos(phis[nd])
            yy[nd] = r*np.sin(phis[nd])

        # add the average point
        cx, cy = self.calc_phase_center(phis)

        # setup the plot
        ax = self.get_plot_ax()
        ax.clear()
        ax.set_aspect(1)
        ax.axis('off')

        # oscillator points
        ax.set_title('phases  (t='+str(np.round(t, 1))+')')
        ax.set_xlim(-1-delta_r, 1+delta_r)
        ax.set_ylim(-1-delta_r, 1+delta_r)

        # center and order parameter point
        ax.scatter(xx, yy, s=5, c=colors)
        ax.scatter(0, 0, c='k', s=2)
        ax.scatter(cx, cy, c=[(0.2, 1, 0.1)], s=7)
        
        plt.pause(0.05)

    def node_colors_by_frequency(self):
        '''
        return list of colors corresponding to the nodes:
        red - fast, blue - slow
        '''
        scales = np.array([self.w[nd] for nd in range(self.n)])
        scales -= min(scales)

        if max(scales) > 10**-6:
            scales /= max(scales)
        else:
            scales += 0.5

        colors = [(x**0.5, 0.5*(1-x), 0.8*(1-x)) for x in scales]
        return colors
    
