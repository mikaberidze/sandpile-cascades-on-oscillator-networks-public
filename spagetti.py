#%%
from mod_KP import KuraPile
from mod_SP import Sandpile
from mod_WH import WorkHorse
from tqdm import tqdm
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import scipy


def paper_plot_prep_1(with_scilimits=True):
    plt.rcParams['font.size'] = 9
    plt.rcParams['axes.linewidth'] = 1
    fig = plt.figure(figsize=(4, 2.5))
    if with_scilimits:
        plt.ticklabel_format(style='sci', axis='both', scilimits=(-1,2))
    # plt.figure(fig.number)
    
def paper_plot_prep_2(with_scilimits=True):
    plt.rcParams['font.size'] = 9
    plt.rcParams['axes.linewidth'] = 1
    fig = plt.figure(figsize=(2, 3))
    if with_scilimits:
        plt.ticklabel_format(style='sci', axis='both', scilimits=(0,0))
    # plt.figure(fig.number)
        
def paper_plot_prep_3(with_scilimits=True):
    plt.rcParams['font.size'] = 10
    plt.rcParams['axes.linewidth'] = 1
    fig = plt.figure(figsize=(3.5, 2.5))
    if with_scilimits:
        plt.ticklabel_format(style='sci', axis='both', scilimits=(0,3))
    # plt.figure(fig.number)
    
def save_fig(name='blank'):
    plt.savefig(name+'.eps', format='eps')

#%%
# Fig. 2a, 2b - parameter swipes
wh_alpha = WorkHorse.load('WH 3-regular, n=4000; KP topple->rnd φ, local(km) punishing dc_dr=var, ΔT=1; KM k=1.0, ω=0; t_run=30000.0; SP dissip=0.005, c₀=5 (merger of 15)')
wh_k = WorkHorse.load('WH 3-regular, n=4000; KP topple->rnd φ, local(km) punishing dc_dr=1, ΔT=1; KM k=var, ω=0; t_run=30000.0; SP dissip=0.005, c₀=5 (merger of 5)')

paper_plot_prep_2(False)
wh_alpha.lable_font_size = 10
wh_alpha.ylabels = ['','','','(I)','(II)','(III)','','','']
wh_alpha.xlabel = '$\\alpha$'
wh_alpha.plot_as_subplots([3,4,5])
plt.savefig('1a')
paper_plot_prep_2(False)
wh_k.lable_font_size = 10
wh_k.ylabels = ['','','','(I)','(II)','(III)','','','']
wh_k.xlabel = '$k$'
wh_k.plot_as_subplots([3,4,5])
plt.savefig('1b')

#%%
# Fig. 3a, 3b - cascade area distrib for varying probability and size
nn = [1000,2000,4000,8000,16000]
kps_nn = []
for n in tqdm(nn):
    kp = KuraPile.load(f"kp n={n}, p=20_n, (merger)")
    kps_nn.append(kp)
    
paper_plot_prep_3()
for kp,n in zip(kps_nn,nn):
    exp = len(str(n))-1
    coef = n/10**exp
    if coef == int(coef): coef = int(coef)
    kp.sp.plot_cascade_area_distr(label=f'$N={coef}\\times 10^{exp}$', error_split=30)
plt.legend(fontsize=8)
plt.ylabel('probability density')


pp = [10,20,40]
kps_pp = []
for p in tqdm(pp):
    name = f"kp n=8000, p={p}_n, (merger)"
    kp = KuraPile.load(name)
    kps_pp.append(kp)

paper_plot_prep_3()
for kp,p in zip(kps_pp,pp):
    kp.sp.plot_cascade_area_distr(label=f'$p={p}/N$', error_split=30)
plt.legend()
plt.ylabel('probability density')


# Fig. 4a, 4b - S, r and area vs. time
name = "kp n=8000, p=20_n, #0"
kp = KuraPile.load_repair_save(name)
kp8k1 = KuraPile.load(name)

paper_plot_prep_3()
kp8k1.plot_r_and_tot_load()
kp8k1.sp.plot_cascade_measure_vs_time(area_size=(True, False),scale=1/8000)
plt.xlabel("time")
plt.ylabel("")
plt.title("")
plt.legend(['$r(t)$','$S$ (scaled)'])
plt.legend(['$r(t)$','$S$ (scaled)', 'cascade area\n(scaled)'])


# Fig. 5a - S vs r (DK cycle)
base = "kp n=8000, p=20_n, #{}"
kps8k2p = [KuraPile.load(base.format(n)) for n in range(10)]

paper_plot_prep_3()
for kp_i in tqdm(kps8k2p):
    kp_i.draw_r_vs_tot_load(color='#1f77b4', ignore_first=3000)
plt.title('')
plt.xlabel("$r$")
plt.ylabel("$S$")
print('phase durations: ', kps8k2p[0].get_regime_durations())


# Fig. 5b - sizes of subsequent cascades
kp8k10 = KuraPile.load("kp n=8000, p=20_n, (merger)")

paper_plot_prep_3()
kp8k10.scatter_last_vs_next_casc_with_r_min(r_min=0, r_max=0.99)
kp8k10.scatter_last_vs_next_casc_with_r_min(r_min=0.1, r_max=0.99, color='orangered')
plt.title('')
plt.xlabel('size of the current cascade')
plt.ylabel('size of the next cascade')


ax2 = plt.gcf().add_axes([0.57, 0.58, 0.32, 0.28])
plt.sca(ax2)
plt.ticklabel_format(style='sci', axis='both', scilimits=(0,0))
kp8k1.sp.plot_cascade_measure_vs_time(area_size=(False, True), decorate=False)
plt.yscale('log')

# Fig. 5c - sizes of subsequent cascades for BTW SP
sp = Sandpile.load('SP 3-regular, n=8000; SP dissip=0.0025, c(i)=2, <s>_node=1.498')
paper_plot_prep_3()
sp.scatter_last_vs_next_casc()


# Fig. 6a - seed histogram as a func. of r
paper_plot_prep_3()
# kp8k10.seed_data_hist('r')
kp8k10.seed_hist_by_phases()
plt.title('')
plt.xlabel('$r$')
plt.ylabel('seed count')
## plt.yscale('log')
ax2 = plt.gcf().add_axes([0.4, 0.33, 0.35, 0.3])
plt.sca(ax2)
plt.yscale('log')
kp8k10.seed_data_hist('r',endo_exo=(True, True), decorate=False)

# Fig. 6b - seed histogram as a func. of distance from last cascade
paper_plot_prep_3()
kp8k10.seed_data_hist('last_casc_dist')
plt.title('')
plt.xlabel('distance to the last cascade')
plt.ylabel('seed count')
# plt.yscale('log')
ax2 = plt.gcf().add_axes([0.5, 0.3, 0.35, 0.3])
plt.sca(ax2)
plt.yscale('log')
kp8k10.seed_data_hist('last_casc_dist', decorate=False)


# Fig. 7 - cascade area distribution for each regime
paper_plot_prep_1()
kp8k10.plot_cascade_measure_distr_for_r(rmin=0.1, rmax=0.9, error_split=30, 
                                area_size=(True, False),
                                exo_seed_present=None, endo_seed_num=None,
                                min_tot_load=7600, max_tot_load=None,
                                color='darkorange')
kp8k10.plot_cascade_measure_distr_for_r(rmin=0.1, rmax=0.9, error_split=30, 
                                area_size=(True, False),
                                exo_seed_present=None, endo_seed_num=None,
                                min_tot_load=None, max_tot_load=7500,
                                color='darkgreen')
kp8k10.plot_cascade_measure_distr_for_r(rmin=0.9, rmax=1, error_split=30, 
                                area_size=(True, False),
                                exo_seed_present=None, endo_seed_num=None,
                                min_tot_load=7500, max_tot_load=9500,
                                color='black')
plt.title('')
plt.ylabel('probability density')
plt.legend(['dragon king','synchronization','buildup'])


# Fig. 8 - total stationary load in SP
def S_theo(n, d, c):
    return c*n*d/2/(d-1)

paper_plot_prep_1()
wh = WorkHorse.load('WH of SP, d=var n=8000, c=2, p=0.0025')
wh.plot_single(0)

n = 8000
c = 2

dd = np.linspace(2.5,9,100)
SS = S_theo(n, dd, c)


plt.plot(dd, SS)
plt.ylabel('$S_c$', fontsize=10)
plt.title(f'd-regular random networks', fontsize=10)
plt.xlabel('$d$', fontsize=10)

plt.legend(['theory', 'simulation'])



# Fig. 9 - DK frequency for globally coupled model
wh8gl = WorkHorse.load('WH 3-regular, n=8000; KP topple->rnd φ, global punishing dc_dr=var, ΔT=1; KM k=1.0, ω=0; t_run=60000.0; SP dissip=0.0025, c₀=5 (merger of 5)')
wh8lc = WorkHorse.load('WH 3-regular, n=8000; KP topple->rnd φ, local(km) punishing dc_dr=var, ΔT=1; KM k=1.0, ω=0; t_run=60000.0; SP dissip=0.0025, c₀=5 (merger of 5)')
paper_plot_prep_1()
wh8gl.ignore_last = 3
wh8lc.ignore_last = 3
wh8gl.plot_single(1)
plot_w_vs_dcdr_theo(n, d, 0, 4.4)
wh8lc.plot_single(1)
plt.legend(['theory','simulation (with $r$)','simulation (with $r_i$)'])
plt.ylabel('Dragon King frequency $f$', fontsize=10)
plt.xlabel('$\\alpha$', fontsize=10)


# Fig. 10 - casc adea distrib during buildup regime
paper_plot_prep_1()
plot(H(poly([0,1])), label='branching process', color='orange')
kploc.plot_cascade_measure_distr_for_r(rmin=0.9, rmax=1, error_split=30, 
                                       min_tot_load=S_min, 
                                       max_tot_load=S_cutoff, color='k',
                                       area_size=(False,True))

plt.legend(['theory','simulation'])
plt.title('')
plt.xlabel('cascade area')

# Fig. 11 - thermodynamic limit
nn = [1000,2000,3000,4000,5000,6000,7000,8000,9000,10000]#,11000,12000,13000,14000,15000,16000]
jobs = 6
ll = [] # Lambda array
ff = [] # frequencies
err = [] # errors
kps_nn_singles = []
for n in tqdm(nn):
    l = n/nn[0]
    ll.append(l)
    fff = []
    for j in range(jobs):
        # kp = KuraPile.load(f"kp n={n}, p=20_n, #0")
        # kp = KuraPile.load(f"kp n={n}, p∝1_log(n), #0")
        kp = KuraPile.load(f"kp n={n}, p=0.0025, #{j}")
        
        fff.append(kp.sp.typical_frequency(improved_resolution=100)[0]*l)
    err.append(scipy.stats.sem(fff))
    ff.append(np.mean(fff))
    

# ff_theo = [w_theo(1000, 3, 1, dT=1)]*len(nn)
ff_trendline = [np.mean(ff)]*len(ll)


paper_plot_prep_1()
plt.errorbar(ll, ff, err, fmt='.')
plt.plot(ll, ff_trendline, '-')
# plt.plot(ll, ff_theo)
plt.xlabel('$\\lambda$')
plt.ylabel('DK frequency $f$')
plt.legend(['linear fit', 'simulation'])
