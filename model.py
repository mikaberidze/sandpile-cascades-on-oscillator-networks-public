#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 23:32:41 2020

@author: guga
"""
from mod_KP import KuraPile
# from common import Saveable
from scipy.stats import binom
from tqdm import tqdm
import matplotlib.pyplot as plt
import numpy as np
from numpy.polynomial.polynomial import Polynomial as poly


order_limit = 3* 10**2

def plot(p, log_log=(True, True)):
    plt.plot(range(1,len(p.coef)), p.coef[1:])
    if log_log[0]:
        plt.xscale('log')
    if log_log[1]:
        plt.yscale('log')
    
def mean(p):
    ww = p.coef
    return sum([w*i for (i,w) in enumerate(ww)])/sum(ww)

# kp = KuraPile.load('3-regular, n=1600; KP dω_ds=0.25, dc_dr=1.1, ΔT=1; KM k=0.333, <r>=0.898, t_run=15000.0; SP dissip=0.0125, <s>_node=2.006')

n = kp.sp.n
p = kp.sp.dissipation_p
d = 3

# q = kp.sp.get_avg_seed_num(endo_exo=(True, True))/n
# S = poly([binom.pmf(i, n, q) for i in range(30)])
S = poly(kp.sp.get_seed_histo(normed=True, endo_exo=(True,True)))


phic = (1-p*d*mean(S))/(1-p)/(d-1)


x = poly([0, 1])
F = poly([1])
for i in tqdm(range(order_limit)):
    F = 1-(1-p)*phic + (1-p)*phic*x*F**(d-1)
    F = F.cutdeg(order_limit)

G = S(x*(F**(d-1)))
G = G.cutdeg(order_limit)

plot(G(poly([0,1])))
# kp.sp.plot_cascade_measure_distrs(log_bins=False)

