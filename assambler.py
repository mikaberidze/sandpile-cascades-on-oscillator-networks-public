#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 11:47:00 2020

@author: guga
"""

from mod_SP import Sandpile
from mod_WH import WorkHorse
from mod_KP import KuraPile
from tqdm import tqdm
import sys


# Check if we are running on the cluster or on the local machine
if len(sys.argv)>1:
    running_on_cluster = True
    inp = int(sys.argv[1])
else:
    running_on_cluster = False
    

def sps():
    n = 32
    params = [0.022]
    base_name = "kp_p_"
    
    for p in params:
        print("param = ", p)
        base = base_name + str(p) + "_"
        sps = [0]*n
        
        print("loading and trimming")
        for i in tqdm(range(n)):
            sps[i] = Sandpile.load(base+str(i)).trim()
        
        print("merging")
        sp = Sandpile.merge(sps, verbose=True)
        
        print("saving...")
        # sp.compute_means_and_errors()
        sp.save("sp_p_" + str(p))
        
def whs():
    n = 15
    base = f'WH 3-regular, n=4000; KP topple->rnd φ, local(km) punishing dc_dr=1, ΔT=1; KM k=var, ω=0; t_run=30000.0; SP dissip=0.005, c₀=5 job#'
    whs = [0]*n
    
    print("loading and trimming")
    for i in tqdm(range(n)):
        whs[i] = WorkHorse.load(base+str(i))
    
    print("merging")
    wh = WorkHorse.merge(whs, verbose=True)
    
    print("saving...")
    wh.compute_means_and_errors()
    wh.save(base[:-5]+f' (merger of {n})')
    
def kps():
    n = 10
    base = f"kp n={inp}, p=0.0025, #"
    kps = [0]*n
    
    print("loading")
    for i in tqdm(range(n)):
        kps[i] = KuraPile.load(base+str(i))
    
    print("merging")
    kp = KuraPile.merge(kps, verbose=True)
    
    print("saving...")
    kp.save(base[:-2]+f' (merger of {n})')
    

whs()

