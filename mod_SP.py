#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 20 14:32:17 2019

@author: guga
"""

import networkx as nx
import random
import matplotlib.pyplot as plt
import numpy as np
import scipy.fftpack
import scipy
from scipy import stats
import copy
from common import Saveable
from time import sleep
from tqdm import tqdm

class Sandpile(Saveable):
    def __init__(self, ng, dissipation_p=0.05, control_p=0, kp=None):
        '''
        initialize sandpile model

        Parameters
        ----------
        ng : NetworkGenerator
            background network.
        dissipation_p : float, optional
            probability of sand being dissipated while being shed.
        '''
        self.ng = ng
        self.net = ng.net
        self.n = self.net.number_of_nodes()
        self.kp = kp
        
        # recorded history
        self.cascades = [] # (area, size, lifetime)
        self.tot_load = []
        self.cascade_seeds = [] # (exogenous, [endogenous])
        self.load_distrib = []
        
        self.cascade_monitor = True
        self.control_p = control_p

        self.grains = {node: 0 for node in self.net.nodes()}
        self.cap = {node: 0 for node in self.net.nodes()}  # node capacities
        self._number_of_grains = 0  # total amount of sand in the network
        self.dissipation_p = dissipation_p
        self.nx_drawer = nx.draw_spectral  # or nx.draw_kamada_kawai
        
        self.cap_info = None
        self.additional_info = None
        self.ax = None  # plt axis to print to
        self.record_state_matrix = False

    def set_node_cap(self, node, cap):
        self.cap[node] = cap
        self.cap_info = None

    def get_node_cap(self, node):
        return self.cap[node]

    def set_node_caps_by_degree(self):
        for nd in self.net.nodes():
            self.set_node_cap(nd, self.get_node_degree(nd)-1)
        self.cap_info = ", c(i)=deg(i)-1"

    def set_all_node_caps(self, caps):
        for nd in self.net.nodes():
            self.set_node_cap(nd, caps)
        self.cap_info = ", c(i)="+str(caps)

    def get_node_degree(self, node):
        return len(self.net.edges(node))

    def add_grains_randomly(self, m):
        '''
        adds m sand grains total randomly to the nodes
        does not add grains to the nodes that are already at capacity
        '''
        # check if the user is too gready with m
        if m+self._number_of_grains > self.get_max_stable_capacity():
            raise Exception("too many grains requested")

        while m > 0:
            node = self.random_node()
            if not self.add_grain(node):
                m -= 1
            else:
                self.remove_grain(node)

    def add_grain_to_rnd_node(self, animate=False):
        '''
        adds a single grain to a randomly chosen node even if it's at capacity
        return the chosen node
        '''
        node = self.random_node()
        self.add_grain(node)

        if animate:
            self.draw(new_grain_nd = node)

        return node

    def add_grain(self, node, m=1):
        '''
        adds m grains to the specified node.
        Return true if it is over capacity, false otherwise
        '''
        self.grains[node] += m
        self._number_of_grains += m

        if self.node_is_over_cap(node):
            return True
        else:
            return False
        
    def remove_grain(self, node, m=1):
        ''' pop m grains from the specified node '''
        if self.grains[node] >= m:
            self._number_of_grains -= m
            self.grains[node] -= m
        else:
            raise Exception("no more grains to remove")

    def remove_grain_randomly(self, animate=False):
        if self.tot_load[-1]==0:
            raise Exception("trying to remove load from empty system")
        node = self.random_node()
        
        while self.grains[node]==0:
            node = self.random_node()
        self.remove_grain(node)

        if animate:
            self.draw(new_grain_nd = node)

        return node

    def move_a_random_grain(self, animate=False):
        '''
        move a grain from random node to its neighbor
        Returns True if this pushed the neighbor over capacity
        '''
        if len(self.net.edges()) == 0:
            raise Exception("no edges in the network,\
                            impossible to move grains around")
        if self.number_of_grains() == 0:
            raise Exception("no grains in the network to move around")

        nd1 = self.random_node()
        while self.node_is_empty(nd1) or len(self.node_neighbours(nd1)) == 0:
            nd1 = self.random_node()
        self.remove_grain(nd1)

        nd2 = random.choice(self.node_neighbours(nd1))
        over_cap = self.add_grain(nd2)

        if animate:
            self.draw()

        return over_cap

    def cascade(self, animate=False, show_cascade_evolution=True, 
                timeout=None):
        '''
        initiate cacade of topplings
        if timeout is not None, it is the number of iterations after which the 
        cascade will finish even if nodes are unstable

        Return: (a, s)
        a - cascade area (# of distinct failed nodes) and
        s - cascade size (total # of failures)
        in case of DK (n, -1)

        '''
        fails = self.failing_nodes()

        self.failed_nodes_in_last_cascade = set()
        conductors_of_cascade = set()
        failure_size = 0
        cascade_t = 0
        if self.cascade_monitor:
            self.tot_load.append(self.number_of_grains())
            self.cascades.append([0, 0, 0])
            self.log_state()
        while len(fails) > 0:
            # keep the record
            self.failed_nodes_in_last_cascade.update(fails)
            failure_size += len(fails)
            cascade_t += 1
            conductors_of_this_topple = set()

            # topple and get failed edges
            for node in fails:
                conductors_of_this_topple.update(self.topple(node))

            # draw
            if animate and show_cascade_evolution:
                self.draw(conductors_of_cascade=conductors_of_cascade,
                          conductors_of_this_topple=conductors_of_this_topple)
                if self.kp is not None:
                    self.kp.update_km(animate=animate)

            # reset the cycle
            fails = self.failing_nodes()
            conductors_of_cascade.update(conductors_of_this_topple)

            # check if timeout has been reached
            if timeout is not None:
                timeout -= 1
                if timeout<=0:
                    break

        # draw
        if animate:
            self.draw(conductors_of_cascade=conductors_of_cascade,
                          conductors_of_this_topple=[])
            if self.kp is not None:
                self.kp.update_km(animate=animate)
            if hasattr(self,"plp"):
                self.plp.reset_edge_colors()

        area_size_t = [len(self.failed_nodes_in_last_cascade), failure_size, 
                     cascade_t]
        if self.cascade_monitor:
            self.cascades[-1] = area_size_t
            self.record_load_distribs()
            
        # return the (area, size) of the cascade
        return area_size_t

    def topple(self, node):
        '''
        topples the given node, sand sheds to neighbors or dissipates
        Return list of edges that have conducted the sand
        '''
        if not self.node_is_over_cap(node):
            raise Exception("false panik, the toppled node wasn't failing")

        conductors = set()
        neighbors = self.node_neighbours(node)
        random.shuffle(neighbors)
        # send out capacity number of grains (not the whole content of node)
        grains = int(self.cap[node]) + 1

        self.remove_grain(node, m=grains)
        control_rand = random.random()
        
        for gr in range(grains):
            if random.random() < self.dissipation_p:
                continue
            
            neighbor = neighbors[gr % len(neighbors)]
            
            if control_rand < self.control_p: 
                for n in neighbors:  #send to most vacant neighbor
                    if self.vacant_spots(n) > self.vacant_spots(neighbor):
                        neighbor = n
                #send to whoever already toppled (parent)
                if self.vacant_spots(neighbor)==0: 
                    for n in neighbors:
                        if n in self.failed_nodes_in_last_cascade:
                            neighbor = n
                    # if none toppled yet send all to 1
                    if neighbor not in self.failed_nodes_in_last_cascade:
                        neighbor = neighbors[0]
    
            self.add_grain(neighbor)
            conductors.update([(node, neighbor), (neighbor, node)])
        
        return conductors
    
    def vacant_spots(self, node):
        return max(int(self.cap[node]-self.grains[node]),0)
    
    def drop_and_casc(self, n=1, verbose=False, animate=False, 
                      show_cascade_evolution=True):
        rng = range(n)
        if verbose and n>0:
            print("starting sandpile drop and cascade")
            sleep(0.5)
            rng = tqdm(rng, leave=False, mininterval=0.5)
        for i in rng:
            nd = self.add_grain_to_rnd_node(animate=animate)
            self.record_seeds(nd)
            self.cascade(animate=animate, show_cascade_evolution=
                         show_cascade_evolution)
    
    def relax(self, verbose=False):
        ''' randomly adds grains and cascades N times without recording.
        Here N is set equal to the total capacity of SP '''
        relaxation_iters = self.get_max_stable_capacity()

        was_recording = self.cascade_monitor
        self.cascade_monitor = False

        rng = range(relaxation_iters)
        if verbose:
            print("\n relaxing the sandpile")
            sleep(0.5)
            rng = tqdm(rng)

        for i in rng:
            self.add_grain_to_rnd_node()
            self.cascade()

        if verbose:
            print("\n")
            sleep(0.5)

        self.cascade_monitor = was_recording
        
    def record_seeds(self, exo_nd):
        if self.cascade_monitor:
            fails = self.failing_nodes()
            
            if exo_nd in fails: 
                fails.remove(exo_nd)
                self.cascade_seeds.append((exo_nd, fails))
            else:
                self.cascade_seeds.append((None, fails))
                
    def record_load_distribs(self):
        ''' record load distribution '''
        distrib_map = {}
        for node in self.net.nodes():
            s = self.grains[node]
            if s in distrib_map:
                distrib_map[s] += 1
            else:
                distrib_map[s] = 1
        max_load = max([s for s in distrib_map])
        distrib = [0]*(max_load+1)
        for s in distrib_map:
            distrib[s] = distrib_map[s]/self.n
            
        self.load_distrib.append(distrib)
    
    def log_state(self):
        ''' record the loads as a new row in an output matrix '''
        file = open("Out/sp_state.txt", "a")
        first = True
        for nd in self.net.nodes():
            if first:
                file.write(str(self.grains[nd]))
                first = False
            file.write(", " + str(self.grains[nd]))
        file.write('\n')
        file.close()
    
    def node_neighbours(self, node):
        return [nd for nd in self.net.neighbors(node)]

    def failing_nodes(self):
        ''' returns the list of nodes that are currently over cap'''
        return [node for node in range(self.n) if self.node_is_over_cap(node)]

    def get_activity(self):
        ''' returns fraction of nodes that are unstable '''
        return len(self.failing_nodes())/self.n

    def nodes_at_capacity(self):
        ''' returns the list of nodes that are currently at cap'''
        return [node for node in range(self.n) \
                if self.grains[node]==self.get_node_cap(node)]

    def node_is_over_cap(self, node):
        return self.grains[node] > self.get_node_cap(node)

    def node_is_empty(self, node):
        return self.grains[node] == 0

    def get_max_stable_capacity(self):
        ''' total max sand capacity of the network '''
        return sum([int(self.cap[nd]) for nd in self.net.nodes()])

    def random_node(self, unsaturated=False):
        return random.choice(range(self.n))

    def number_of_grains(self):
        ''' current number of grains '''
        return self._number_of_grains
    
    def trim(self):
        ''' trim the sandpile data down to its relaxed part only '''
        start = self.saturation_iter()
        
        self.cascades = self.cascades[start :]
        self.tot_load = self.tot_load[start :]
        self.cascade_seeds = self.cascade_seeds[start :]
        self.load_distrib = self.load_distrib[start :]
        
        return self
    
    def __str__(self):
        if hasattr(self, 'name'):
            return 'SP extract from KP; ' + self.name
        name = "SP dissip=" + str(self.dissipation_p)
        if self.cap_info is not None:
            name += self.cap_info
        if len(self.tot_load)>0:
            try: name += ", <s>/node=" + \
                    ("%.3f"%(self.avg_number_of_grains()/self.n))
            except: name += ", SP didn't saturate"
        if self.control_p!=0:
            name += ", control_p=" + ("%.3f"%self.control_p)
        if hasattr(self, 'additional_info') and \
                                            self.additional_info is not None:
            name += ", " + self.additional_info
        
        return name

    
#------------------------------------------------------------------------ Plotters
    def set_plot_ax(self, ax, fig):
        self.ax = ax
        ax.axis('off')

    def get_plot_ax(self):
        if self.ax is None:
            self.ax = plt.axes()

        return self.ax

    def draw(self, fig=1, conductors_of_cascade=set(), new_grain_nd=None,
             conductors_of_this_topple=set(), with_labels=False):
        ''' draw the network and color the nodes & edges correspondingly '''
        
        if hasattr(self,'plp'):
            self.plp.loads = self.grains
            if new_grain_nd is not None:
                self.plp.add_special_loads(new_grain_nd, 1)
            self.plp.color_edges(conductors_of_cascade, "black")
            self.plp.color_edges(conductors_of_this_topple, "darkred")
            s = self._number_of_grains
            if hasattr(self,'expected_load_bounds'):
                smin, smax = self.expected_load_bounds
            else:
                smin, smax = (0, self.get_max_stable_capacity())
            bar = (s-smin)/(smax-smin)
            bar = max(0, min(1,bar))
            self.plp.include_load_saturation(bar)
            self.plp.plot_2d()
            self.plp.reset_special_loads()
            return
        
        node_colors = [self.color_of_node(node) for node in self.net.nodes()]
        edge_colors = [(1, 0, .1) if edge in conductors_of_this_topple
                       else (.5, .5, .5) if edge in conductors_of_cascade
                       else (.8, .8, .8) for edge in self.net.edges()]

        ax = self.get_plot_ax()
        ax.clear()
        self.nx_drawer(self.net, with_labels=with_labels,
                       node_color=node_colors, node_size=15,
                       edge_color=edge_colors, width=1.5,
                       ax=ax)

        plt.pause(0.01)

    def plot_tot_sand(self, dt=1, scale=1):
        tt = [i*dt for i in range(len(self.tot_load))]
        label = 'total load'
        if scale!=1:
            label += '/{:.0f}'.format(1/scale)
        plt.plot(tt, np.array(self.tot_load)*scale, label=label)
        plt.xlabel('time')
        plt.ylabel('total amount of sand')
        plt.title(str(self), fontsize=10)
        # plt.ylim(0, 1)
        plt.show()

    def plot_load_distribs_vs_t(self, dt=1):
        tt = [i*dt for i in range(len(self.tot_load))]
        
        length = max(map(len, self.load_distrib))
        y=np.array([xi+[0]*(length-len(xi)) for xi in self.load_distrib])
        for s in range(length):
            plt.plot(tt, y[(len(y[:,s]) - len(tt)):,s], 
                      label='prob. of {} grains in node'.format(s))
        plt.xlabel('time')
        plt.title(str(self), fontsize=10)
        # plt.ylim(0, 1)
        plt.show()
        plt.legend()
        
    def plot_load_distribs_vs_S(self, j=None):
        ''' j is the specific load for which the concentration rho is plotted
        if j=None, than all are plotted '''
        length = max(map(len, self.load_distrib))
        y=np.array([xi+[0]*(length-len(xi)) for xi in self.load_distrib])
        for s in range(length):
            if j is None or j==s:
                plt.scatter(self.tot_load, 
                            y[(len(y[:,s]) - len(self.tot_load)):,s],
                            marker='.', s=.1, 
                            label=f'prob. of {s} grains in node')
        plt.title(str(self), fontsize=10)
        # plt.ylim(0, 1)
        plt.show()
        plt.legend()
        
    def plot_endo_seeds_num_vs_t(self, scale=1):
        tt = [i for i in range(len(self.tot_load))]
        plt.plot(tt, np.array(
            [len(self.cascade_seeds[t][1]) for t in tt]
            )*scale, 
            label='# of endo seeds' + 
                ('' if scale==1 else '/{:.0f}'.format(1/scale))
            )
        plt.xlabel('time')
        plt.ylabel('number of endo seeds')
        plt.title(str(self), fontsize=10)
        # plt.ylim(0, 1)
        plt.show()
        
    def scatter_cascade_measure_vs_endo_num(self, area_size=(False, True)):
        area, size = area_size
        endo_seeds = np.array(self.cascade_seeds)[:,1]
        endo_seeds_num = [len(seeds) for seeds in endo_seeds]
        if area:
            plt.scatter(np.array(self.cascades)[:,0], endo_seeds_num, s=1,
                        label='cascade area')
        if size:
            plt.scatter(np.array(self.cascades)[:,1], endo_seeds_num, s=1,
                        label='cascade size')
        plt.xlabel('cascade measure')
        plt.ylabel('# of endogenous seeds')
        plt.legend()
        plt.title(str(self), fontsize=10)
        
    def plot_sand_per_node(self):
        self.plot_tot_sand(scale = 1/self.n)
        plt.ylabel('amount of sand per node')
            
    def plot_cascade_measure_vs_tot_sand(self):
        ''' 
        scatter plot: 
        X: tot sand before the given cascade
        Y: measure of given cascade
        '''
        casc = np.array(self.cascades)

        plt.scatter(self.tot_load, casc[:,0]/self.n, marker='.', s=2)
        shiftedx = np.array(self.tot_load)+0.5
        plt.scatter(shiftedx, casc[:,1]/self.n, marker='.', s=2)
       
        plt.xlabel('total load of sand before the cascade')
        plt.ylabel('relative cascade measure')
        plt.show()
        
    def plot_avg_measure_vs_tot_sand(self, bin_size=10):
        ''' 
        scatter plot: 
        X: tot sand before the given cascade
        Y: measure of given cascade
        '''
        casc = np.array(self.cascades)
        
        max_load = max(self.tot_load)+1
        
        borders = np.linspace(0, max_load, int(max_load/bin_size))
        bin_size = borders[1]
        
        areas = [0]*(len(borders)-1)
        sizes = [0]*(len(borders)-1)
        count = [0]*(len(borders)-1)
        
        for (a,s,t),l in zip(self.cascades, self.tot_load):
            bn = int(l/bin_size)
            areas[bn] += a
            sizes[bn] += s
            count[bn] += 1
        
        for bn in range(len(count)):
            areas[bn] /= count[bn]*self.n if count[bn]>0 else 1
            sizes[bn] /= count[bn]*self.n if count[bn]>0 else 1
            
        xx = np.array(borders[:-1])+bin_size/2
        
        plt.plot(xx, areas, label="avg area")
        plt.plot(xx, sizes, label="avg size")
       
        plt.xlabel('total load before the cascade')
        plt.ylabel('cascade measure')
        plt.show()
        
    def plot_tot_sand_FT(self, label=None, scaled=True, improved_resolution=1):
        freq, ampl = self.tot_sand_FT(improved_resolution=improved_resolution)
        if scaled:
            ampl /= max(ampl)
        plt.plot(freq, ampl, label=label)
        plt.xlabel('frequency')
        plt.ylabel('amplitude')
        plt.title('sand content Fourier Transform')
        plt.show()
        
    @staticmethod
    def _plot_log_histo_err(ss, x_lbl, y_lbl, log_bins=True, error_split=30,
                            label=None, color=None):
        ''' plot hist of ss integers. Bins start at 1 and end at ~ max value 
        of ss. data is split into error_split parts and STE is calculated '''
        ss_max = max(ss)
        random.shuffle(ss)
        sss = Sandpile._chunk_it(ss, error_split)
        sss = [sorted(ss) for ss in sss]

        # bins = np.logspace(1, np.log10(ss_max), num=bin_num)
        if log_bins:
            bins = np.logspace(0, np.ceil(np.log2(ss_max)), 
                               num=1+int(np.ceil(np.log2(ss_max))), 
                               base=2)-0.5
        else:
            bins = np.array(range(ss_max))+0.5
        
        probabilities = [0]*error_split
        
        for (j, ss) in enumerate(sss):
            probabilities[j] = np.histogram(ss, bins=bins, density=True)[0]
        
        
        probabilities = np.array(probabilities)
        x = [(bins[i]+bins[i+1])/2 for i in range(len(bins)-1)]
        if error_split>1:
            y = np.mean(probabilities, axis=0)
            err = stats.sem(probabilities, axis=0)
        else:
            y = probabilities[0]
            err = None
        
        if log_bins:
            style = {'fmt':'o-', 'alpha':0.6, 'linewidth':2}
        else:
            style = {'fmt':'.-', 'markersize':1, 'linewidth':0.1}
        if color is not None:
            style['color']=color
        plot = plt.errorbar(x, y, yerr=err, **style, label=label)

        plt.xscale("log")
        plt.yscale("log")

        plt.xlabel(x_lbl)
        plt.ylabel(y_lbl) 
        
        return x, y, err, plot

    @staticmethod
    def _chunk_it(a, n):
        ''' split list a into n parts of approx equal length '''
        k, m = divmod(len(a), n)
        out = [a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] \
               for i in range(n)]
        if n != len(out): 
            raise Exception("chunk_it fucked up")
        return out
    
    def log_slope(self, sl=-1.5, x_max=None, offset=10, color=None):
        if x_max is None:
            x_max = max(self.cascades)[1]
        xx = np.logspace(0, np.log10(x_max), 5)
        slope = [x**(sl)*offset for x in xx]
        plt.loglog(xx, slope, '--', label=f"slope={sl}", color=color)
        
    def plot_cascade_measure_vs_time(self, scale=1, area_size=(True, False),
                                     decorate=True): 
        tt = [i for i in range(len(self.cascades))]
        yy = np.array(self.cascades)*scale
        area, size = area_size
        if area:
            plt.plot(tt, yy[:,0], linestyle='-', marker='.',
                label='cascade area ' + 
                    ('' if scale==1 else '/{:.0f}'.format(1/scale))
                )
        if size:
            plt.plot(tt, yy[:,1], linestyle='-', marker='.',
                label='cascade size ' + 
                    ('' if scale==1 else '/{:.0f}'.format(1/scale))
                )
        if decorate:
            plt.xlabel('time')
            plt.ylabel('cascade measure')
            plt.title(str(self), fontsize=10)
        # plt.ylim(0, 1)
        plt.show()
        
    def plot_cascade_area_distr(self, error_split=1, log_bins=True,
                                exo_seed_present=None, endo_seed_num=None,
                                min_tot_load=None, max_tot_load=None,
                                label=None):
        '''plots the histogram of cascade area distribution
        bin_num - number of bins on x axis
        exo_seed_present:
            None - no restriction
            True - only cascades with exogenous seed present
            False - only cascades with no exogenous seed
        endo_seed_num:
            None - no restriction
            int - only cascades with this specific number of endogenous seeds
        
        '''
        lbl = 'area distribution'
        if exo_seed_present is not None:
            lbl += '; exo seed ' + \
                ('present' if exo_seed_present else 'absent')
        if endo_seed_num is not None:
            lbl += '; exactly {} endo seeds'.format(endo_seed_num)
        if min_tot_load is not None:
            lbl += '; min S={}'.format(min_tot_load)
        if max_tot_load is not None:
            lbl += '; max S={}'.format(max_tot_load)
        cascs = self.get_cascades_with(exo_seed_present, endo_seed_num,
                                       min_tot_load, max_tot_load)
        casc_msrs = [a for (a, s, t) in cascs if a>0]
        return self._plot_log_histo_err(casc_msrs, 
                                 "cascade area", "probability",  
                                 log_bins=log_bins, error_split=error_split,
                                 label=lbl if label is None else label)

    def plot_cascade_size_distr(self, error_split=1, log_bins=True,
                                exo_seed_present=None, endo_seed_num=None,
                                min_tot_load=None, max_tot_load=None,
                                label=None):
        '''plots the histogram of cascade size distribution
        bin_num - number of bins on x axis
        exo_seed_present:
            None - no restriction
            True - only cascades with exogenous seed present
            False - only cascades with no exogenous seed
        endo_seed_num:
            None - no restriction
            int - only cascades with this specific number of endogenous seeds
        '''
        cascs = self.get_cascades_with(exo_seed_present, endo_seed_num,
                                       min_tot_load, max_tot_load)
        lbl = 'size distribution'
        if exo_seed_present is not None:
            lbl += '; exo seed ' + \
                ('present' if exo_seed_present else 'absent')
        if endo_seed_num is not None:
            lbl += '; exactly {} endo seeds'.format(endo_seed_num)
        if min_tot_load is not None:
            lbl += '; min S={}'.format(min_tot_load)
        if max_tot_load is not None:
            lbl += '; max S={}'.format(max_tot_load)
        cascs = self.get_cascades_with(exo_seed_present, endo_seed_num,
                                       min_tot_load, max_tot_load)
        casc_msrs = [s for (a, s, t) in cascs if s>0]
        return self._plot_log_histo_err(casc_msrs,
                                "cascade size", "probability", 
                                log_bins=log_bins, error_split=error_split,
                                label=lbl if label is None else label)
        
    def plot_cascade_time_distr(self, error_split=1, log_bins=True,
                                exo_seed_present=None, endo_seed_num=None,
                                min_tot_load=None, max_tot_load=None,
                                label=None):
        '''plots the histogram of cascade lifetime distribution
        bin_num - number of bins on x axis
        exo_seed_present:
            None - no restriction
            True - only cascades with exogenous seed present
            False - only cascades with no exogenous seed
        endo_seed_num:
            None - no restriction
            int - only cascades with this specific number of endogenous seeds
        '''
        cascs = self.get_cascades_with(exo_seed_present, endo_seed_num,
                                       min_tot_load, max_tot_load)
        lbl = 'lifetime distribution'
        if exo_seed_present is not None:
            lbl += '; exo seed ' + \
                ('present' if exo_seed_present else 'absent')
        if endo_seed_num is not None:
            lbl += '; exactly {} endo seeds'.format(endo_seed_num)
        if min_tot_load is not None:
            lbl += '; min S={}'.format(min_tot_load)
        if max_tot_load is not None:
            lbl += '; max S={}'.format(max_tot_load)
        cascs = self.get_cascades_with(exo_seed_present, endo_seed_num,
                                       min_tot_load, max_tot_load)
        casc_msrs = [t for (a, s, t) in cascs if t>0]
        return self._plot_log_histo_err(casc_msrs,
                                "cascade lifetime", "probability", 
                                log_bins=log_bins, error_split=error_split,
                                label=lbl if label is None else label)
        
    def plot_cascade_measure_distrs(self, error_split=1, log_bins=True,
                                exo_seed_present=None, endo_seed_num=None,
                                min_tot_load=None, max_tot_load=None):       
        self.plot_cascade_area_distr(error_split, log_bins, exo_seed_present,
                                     endo_seed_num, min_tot_load, max_tot_load)
        self.plot_cascade_size_distr(error_split, log_bins, exo_seed_present, 
                                     endo_seed_num, min_tot_load, max_tot_load)   
        self.plot_cascade_time_distr(error_split, log_bins, exo_seed_present, 
                                     endo_seed_num, min_tot_load, max_tot_load)       
    
    def plot_load_histo(self):
        arr = [self.grains[nd] for nd in self.net.nodes()]
        self.plot_histo(arr, xlabel="load on single node",\
                        ylabel="probability")
        
    def plot_seed_histo(self, endo_exo=(True, False)):
        endo, exo = endo_exo
        seeds = np.array([0]*len(self.cascade_seeds))
        if endo:
            seeds += np.array([len(ss) for (exo, ss) in self.cascade_seeds])
        if exo:
            seeds += np.array([0 if exo is None else 1\
                     for (exo, ss) in self.cascade_seeds])
            
        self.plot_histo(seeds, xlabel="number of cascade seeds",\
                        ylabel="probability")
        title = ''
        if endo and exo: title = 'all seeds'
        elif endo: title = 'endogenous seeds'
        elif exo: title = 'exxogenous seeds'
        plt.title(title)
        
        return np.mean(seeds)
    
    def scatter_last_vs_next_casc(self, area_size=(False, True), scale=1):
        area, size = area_size
        cascs = np.array(self.cascades)*scale
        if area:
            plt.scatter(cascs[:-1,0], cascs[1:,0], s=1, label='areas')
        if size:
            plt.scatter(cascs[:-1,1], cascs[1:,1], s=1, label='sizes')
        plt.xlabel('size of the current cascade')
        plt.ylabel('size of the next cascade')
    
    def scatter_time_vs_size(self):
        cascs = np.array(self.cascades)
        plt.scatter(cascs[:,1], cascs[:,2], s=1)
        plt.xlabel('size ')
        plt.ylabel('lifetime')
        plt.xscale('log')
        plt.yscale('log')
    
    @staticmethod
    def plot_histo(arr, xlabel="", ylabel="", title="", bin_size=1):
        ''' Plots normalized histogram of given array '''
        arr = [a for a in arr]
        bins = np.arange(0, max(arr)+1.5*bin_size, bin_size) - 0.5*bin_size
        weights = np.ones_like(arr)/float(len(arr))
        plt.hist(arr, weights=weights, bins=bins, alpha=0.6, linewidth=1.2)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.title(title)
        plt.show()
        
    def plot_load_distribution(self):
        ''' plot the sand distribution histogram '''
        histo = self.get_grain_histo()

        alpha_i = [i for i in range(len(histo))]

        p = plt.plot(alpha_i, histo, '-o', label='load distribution')
        plt.show()
        plt.xlabel("s (# of sand grains)")
        plt.ylabel("# of nodes with s grains in it")
        return p
    
    def plot_average_seed_distances(self):
        ''' plot average endogenous seed distances '''
        max_seeds = 0
        for (exo, seeds) in self.cascade_seeds:
            max_seeds = max(max_seeds, len(seeds))
        
        seed_dist = []
        seed_err = []
        rand_dist = []
        rand_err = []
        
        ss = list(range(2, max_seeds+1))
        for s in ss:
            sd, se = self.avg_seed_distance(s)
            seed_dist.append(sd)
            seed_err.append(se)
            rd, re = self.avg_rand_distance(s)
            rand_dist.append(rd)
            rand_err.append(re)
        
        plt.errorbar(ss, seed_dist, seed_err)
        plt.errorbar(ss, rand_dist, rand_err)
        
        plt.legend(["average distance between cascade seeds",\
                    "average distance between randomly chosen nodes"])
        plt.xlabel("number of nodes (seeds or randomly chosen nodes)")
        plt.ylabel("average distance")
        
        plt.show()
    
    def color_of_node(self, node):
        x = self.grains[node]/(self.get_node_cap(node)+1)
        if x > 1:
            x = 1
        return (x**0.5, 0.5*(1-x), 0.8*(1-x))
    
    
#------------------------------------------------------------------------ Analysis
    def get_cascades_with(self, exo_seed_present=None, endo_seed_num=None,
                          min_tot_load=None, max_tot_load=None, 
                          with_indices=False):
        '''
        returns list of all cascades that satisfy the following criteria
        exo_seed_present:
            None - no restriction
            True - only cascades with exogenous seed present
            False - only cascades with no exogenous seed
        endo_seed_num:
            None - no restriction
            int - only cascades with this specific number of endogenous seeds
        '''
        tot_loads = self.tot_load
        cascs = self.cascades
        seeds = self.cascade_seeds
        
        cc = []
        ii = []
        for i,(c, (exo, endo), tot_load) in \
                                    enumerate(zip(cascs, seeds, tot_loads)):
            if c[1] > 0 \
                    and (endo_seed_num is None or len(endo)==endo_seed_num) \
                    and (exo_seed_present is None or \
                         (exo is not None)==exo_seed_present) \
                    and (min_tot_load is None or tot_load>min_tot_load) \
                    and (max_tot_load is None or tot_load<max_tot_load):
                cc.append(c)
                ii.append(i)
        
        if with_indices:
            return cc, ii
        return cc
    
    def fit_power_law(self, hist_out, trim_l=3, trim_r=3):
        ''' fits the log-log histo with a power law. returns the 
        power law exponent '''
        if trim_r==0: 
            trim_r=None
        else:
            trim_r = -trim_r
        x, y, col = hist_out[0], hist_out[1], hist_out[3][0].get_color()
        
        def func(x, a, b):
            return a*x+b
        
        popt, pcov = scipy.optimize.curve_fit(func, 
                    np.log(x[trim_l:trim_r]), np.log(y[trim_l:trim_r]))
        a, b = popt
        
        plt.plot(x, np.e**func(np.log(x),a,b),
                 '--', linewidth=1, color=col, 
                 label='power law={:.3f}'.format(a))
        
        return a

    def fit_stretched_exp(self, hist_out, trim_l=0, trim_r=0):
        ''' fits log log histo with stretched exponential distr.
        returns c and x0 '''
        if trim_r==0: 
            trim_r=None
        else:
            trim_r = -trim_r
        x, y, col = hist_out[0], hist_out[1], hist_out[3][0].get_color()
        
        def func(lnx, c, x0):
            return np.log(c)-c*np.log(x0)+(c-1)*lnx-np.e**(c*lnx-c*np.log(x0))
        print('x', x)
        print(np.log(x[trim_l:trim_r]), np.log(y[trim_l:trim_r]))
        popt, pcov = scipy.optimize.curve_fit(func, 
                    np.log(x[trim_l:trim_r]), np.log(y[trim_l:trim_r]))
        c, x0 = popt
        
        xx = np.logspace(np.log(min(x)), np.log(max(x)), 50, base=np.e)
        plt.plot(xx, np.e**func(np.log(xx), c, x0),
                 '--', linewidth=1, color=col, 
                 label='stretched exp (c={:.3f}, x₀={:.3f})'.format(c, x0))
        
        return (c, x0)
    
    def fit_log_normal(self, hist_out, trim_l=0, trim_r=0):
        ''' fits log log histo with log-normal distr. returns mu and sigma '''
        if trim_r==0: 
            trim_r=None
        else:
            trim_r = -trim_r
        x, y, col = hist_out[0], hist_out[1], hist_out[3][0].get_color()
        
        def func(lnx, sigma, mu):
            lnp = -lnx-np.log(2*np.pi)/2-np.log(sigma)-(lnx-mu)**2/(2*sigma**2)
            return lnp
        
        popt, pcov = scipy.optimize.curve_fit(func, 
                    np.log(x[trim_l:trim_r]), np.log(y[trim_l:trim_r]))
        sigma, mu = popt
        
        xx = np.logspace(np.log(min(x)), np.log(max(x)), 50, base=np.e)
        plt.plot(xx, np.e**func(np.log(xx), sigma, mu),
                 ':', linewidth=1, color=col, 
                 label='log normal (σ={:.3f}, μ={:.3f})'.format(sigma, mu))
        
        return (sigma, mu)

    def avg_number_of_grains(self, k=-1):
        ''' 
        average sand content in the net since iteration k. If k unspecified, 
        it chooses k after whicch system seems to have relaxed
        '''
        if k < 0:
            k = self.saturation_iter()
        return np.mean(self.tot_load[k:])
    
    def avg_number_of_grains_per_node(self, k=-1):
        ''' 
        average sand content per node in the net since iteration k. If k
        unspecified, it chooses k after whicch system seems to have relaxed.
        '''
        return self.avg_number_of_grains(k)/self.n
    
    def get_black_swan_probability(self, thresh=0.5):
        ''' share of cascades with relative measure > thresh '''
        thresh_absolute = self.n*thresh
        areas = 0
        sizes = 0
        cascades = self.cascades[self.saturation_iter():]
        for casc in cascades:
            a, s, t = casc
            if a > thresh_absolute:
                areas += 1
            if s > thresh_absolute:
                sizes += 1
        areas = areas/len(cascades)
        sizes = sizes/len(cascades)
        
        return areas, sizes
    
    def get_avg_measures(self):
        ''' average area, size, lifetime '''
        return np.average(self.cascades,0)
    
    def saturation_iter(self):
        ''' The iteration number after which system seems to have relaxed'''
        # last_mean is the average of last 10% of iterations
        last_mean = np.mean(self.tot_load[len(self.tot_load)*9//10:])
        satur = 0
        # find first iter when load crosses last_mean
        while (self.tot_load[satur]-last_mean)*\
            (self.tot_load[satur+1]-last_mean) > 0:
            satur += 1
        if satur/len(self.tot_load) > 0.7:
            raise Exception("SP probably never relaxed")
        return satur

    def get_grain_histo(self):
        '''
        returns histogram of grain distribution:
        list histo[n] = # of nodes that have exactly n grains in them
        '''
        grains_list = [self.grains[nd] for nd in self.net.nodes()]
        histo = [0]*(max(grains_list)+1)

        for a in grains_list:
            histo[a] += 1

        return histo

    def tot_sand_FT(self, improved_resolution=1):
        ''' fourier transform of load vs iter '''
        # Number of samplepoints
        init = self.saturation_iter()
        data = self.tot_load[init:] - self.avg_number_of_grains()
        data = np.concatenate((data, 
                               [0]*int(len(data)*(improved_resolution-1))))
        N = len(data)

        freq = np.linspace(0.0, 1.0/(2.0), N//2)
        ampl = scipy.fftpack.fft(data)
        ampl = 2.0/N * np.abs(ampl[:N//2])

        return freq, ampl

    def typical_frequency(self, improved_resolution=1):
        ''' typical frequency of large cascades (1/sec) '''
        freq, ampl = self.tot_sand_FT(improved_resolution=improved_resolution)
        maxAmpl = 0
        typical_freq = 0

        for i in range(len(freq)):
            if ampl[i] > maxAmpl:
                maxAmpl = ampl[i]
                typical_freq = freq[i]

        return typical_freq, maxAmpl
        
    def get_avg_seed_num(self, endo_exo=(True, False)):
        seed_hist = self.get_seed_histo(endo_exo)
        return sum([i*s for (i, s) in enumerate(seed_hist)]) / sum(seed_hist)

    def get_stable_load_range(self):
        ''' sand load fluctuation range in stable regime '''
        start = self.saturation_iter()
        mx = max(self.tot_load[start:])
        mn = min(self.tot_load[start:])
        return (mn, mx)
        
    def get_seed_histo(self, endo_exo=(True, False), normed=False):
        endo, exo = endo_exo
        seeds = np.array([0]*len(self.cascade_seeds))
        if endo:
            seeds += np.array([len(ss) for (exo, ss) in self.cascade_seeds])
        if exo:
            seeds += np.array([0 if exo is None else 1\
                     for (exo, ss) in self.cascade_seeds])
        return np.histogram(seeds, bins=max(seeds)+1, range=(0, max(seeds)+1),
                            density=True)[0]
    
    def avg_seed_distance(self, n=2):
        '''
        returns the average distance between endogenous seeds and the error
        for events with exactly n endogenous seeds
        '''
        
        seedss = [seedss for (exo_seed, seedss) in self.cascade_seeds 
                  if len(seedss)==n]
        
        if len(seedss)*n*(n-1)/2 < 30:
            print("error bar for n=", n, " is not trustworthy")
        
        dists = []
        
        for seeds in seedss:
            for s in seeds:
                for t in seeds:
                    if s>t:
                        dists.append(len(nx.algorithms.shortest_path( \
                                             self.net, source=s, target=t)))

        return (np.mean(dists), scipy.stats.sem(dists))
        
    def avg_rand_distance(self, n=2):
        ''' returns average distance between randomly chosen n nodes '''
        dists = []
        while len(dists)<100000:
            nodes = random.sample(list(self.net.nodes()), n)
            
            for s in nodes:
                for t in nodes:
                    if s>t:
                        dists.append(len(nx.algorithms.shortest_path( \
                                             self.net, source=s, target=t)))
        return (np.mean(dists), scipy.stats.sem(dists))

    @staticmethod
    def merge(sps, verbose=False):
        ''' merge simmilar sandpiles together '''
        chck = ['n', 'control_p', 'dissipation_p']
        
        merged = copy.deepcopy(sps[0])
        
        mergers = sps[1:]
        if verbose:
            mergers = tqdm(mergers)
        for sp in mergers:
            # check for mergebility
            for attr in chck:
                if merged.__dict__[attr] != sp.__dict__[attr]:
                    raise Exception("can't merge Sandpiles: different ", attr)
                    
            merged.cascades = merged.cascades + sp.cascades
            merged.cascade_seeds = merged.cascade_seeds + sp.cascade_seeds
            merged.tot_load = merged.tot_load + sp.tot_load
        
        if hasattr(sps[0], 'name'):
            merged.name = sps[0].name
        
        return merged
