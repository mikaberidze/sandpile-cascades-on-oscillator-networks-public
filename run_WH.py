#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 12:53:25 2020

@author: guga
"""

from mod_KM import Kuramoto
from mod_KP import KuraPile
from mod_NG import NetworkGenerator
from mod_SP import Sandpile
from mod_WH import WorkHorse
import numpy as np
import sys


# Check if we are running on the cluster or on the local machine
if len(sys.argv)>1:
    running_on_cluster = True
    job = int(sys.argv[1])-1 
else: 
    running_on_cluster = False

#WH 3-regular, n=4000; KP topple->rnd φ, local(km) punishing dc_dr=var, ΔT=1; KM k=1.0, ω=0; t_run=30000.0; SP dissip=0.005, c₀=5

n = 4000
d = 3
c = 5

dissipation_p = 20/n
k = 1.

dc_dr = 1
t_run = lambda n, d : 10*n*d/2/(d-1)
dT = 1
punishing = True
sp_coupling = 'local'# 'global'# 'discrete'#
local_order_param = 'km'# 'our'#

rnd_phases = True
dw_ds = 0.0

name_prefix = ''

# param to sweep over
param_name = 'k' #TODO!
params = np.linspace(0, 5, num=16)# np.logspace(-3,-1,8)# list(range(3,9))# #TODO!


def f(param, save=False, verbose=False):
    globals()[param_name] = param
    ng = NetworkGenerator.random_regular(n, d)
    
    sp = Sandpile(ng)
    # sp.set_node_caps_by_degree()
    sp.set_all_node_caps(c)
    sp.dissipation_p = dissipation_p
    
    km = Kuramoto(ng, k=k)
    km.w_normal_distr(0, 0)
    km.init_phase_uniform_distr(width=np.pi)
    
    kp = KuraPile(km, sp, ng)
    kp.verbose = not running_on_cluster
    kp.collecting_seed_stats = False
    
    kp.punishing = punishing
    if rnd_phases:
        kp.km_update_rule = kp.randomize_km_phases_for_toppled
    else:
        kp.dw_ds = dw_ds
    if sp_coupling=='discrete':
        kp.sp_update_rule = kp.set_sp_caps_discretely
    elif sp_coupling=='global':
        kp.sp_update_rule = kp.set_sp_caps_by_global_r
    else:
        if local_order_param == 'km':
            km.get_local_order_param = km.calc_local_km_order_param
         
    kp.dcap_dr = dc_dr
    kp.sandfall_period = dT
    
    # print("relaxing")
    sp.relax()
    km.relax(50)
    kp.relax(250, verbose=verbose)
    
    # print("collecting cascade data")
    kp.run(t_run(n,d), verbose=verbose)
    
    if save:
        # name = str(kp)
        # sp.kp = None
        # sp.name = name
        kp.save(name_prefix + str(kp)) 
        # pass
        
    load_liminf, load_limsup = kp.sp.get_stable_load_range()
    sp_freq, sp_ampl = kp.sp.typical_frequency()
    km_freq, km_ampl = kp.km.typical_frequency()
    return (kp.cycle_heuristic(),
            sp_freq,
            km_freq,
            kp.sp.get_black_swan_probability()[0],
            kp.sp.avg_number_of_grains_per_node(),
            kp.km.get_avg_order_param(t_run(n,d)/5),
            load_liminf, 
            load_limsup,
            kp.memory_usage()
            )


globals()[param_name] = "var"
wh = WorkHorse(f=f, params=params, iters=2, parallel=running_on_cluster)
wh.f_out_num = 9
wh.first_in_its_kind = (not running_on_cluster) or job==0
wh.compute_data()


wh.name = str(d)+"-regular, n=" + str(n) \
    + ("; KP topple->rnd φ" if rnd_phases else "; KP dω/ds=" + str(dw_ds)) \
    + ", " + sp_coupling \
    + (f"({local_order_param})" if sp_coupling=='local' else '') \
    + (' punishing' if punishing else ' rewarding') \
    + " dc/dr=" + str(dc_dr) + ", ΔT=" + str(dT) \
    + ";\nKM k="+str(k) + ", ω=0; t_run=" + str(t_run(n,d)) \
    + ";\nSP dissip=" + str(dissipation_p) + ", c₀=" + str(c)

wh.xlabel = param_name.replace('n_p','n probability').replace('_','/')
wh.ylabels = ["cycle heuristic",
              "SP typical \nfrequency \n(1/sec)",
              "KM typical \nfrequency \n(1/sec)",
              "Black Swan \nprob. (area>50%)",
              "Average load \nper nodes",
              "KM global \norder param",
              "SP load \nliminf",
              "SP load \nlimsup",
              "memory usage (MB)"
              ]
# wh.ylabels = ["Δφ of endogenous seeds", "Δφ of exogenous seeds"]

if running_on_cluster:
    wh.save(name_prefix + str(wh)+' job#'+str(job))
    print('\n'+str(wh)+' job')
else:
    wh.compute_means_and_errors()
    wh.plot_as_subplots()


# print("watch for RK45 stability")
# kp.animate = True
# kp.run(10)

