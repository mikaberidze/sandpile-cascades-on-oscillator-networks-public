#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 22:00:01 2020

@author: guga
"""
from mod_NG import NetworkGenerator
from mod_SP import Sandpile
from mod_KP import KuraPile
from mod_PLP import PhaseLoadPlot
import matplotlib.pyplot as plt
import sys
from tqdm import tqdm
import random
import numpy as np


# Check if we are running on the cluster or on the local machine
if len(sys.argv)>1:
    running_on_cluster = True
    job = int(sys.argv[1])-1
else:
    running_on_cluster = False

params = [200, 400, 800, 1600, 3200, 6400, 12800, 25600, 51200, 102400]
if running_on_cluster:
    param = params[job%len(params)]
    first_in_its_kind = job//len(params)==0
else:
    param = params[3]


n = 8000
d = 3
caps = d-1
dissip = 20/n
iters = 60000#4*n*caps
control_p = 0
animate = False
verbose = (not running_on_cluster) or first_in_its_kind

# print("Generating a network")
ng = NetworkGenerator.random_regular(n, d)

# print("initializing a Sandpile object")
sp = Sandpile(ng)
sp.dissipation_p = dissip
sp.control_p = control_p
sp.set_all_node_caps(caps)

if animate:
    plp = PhaseLoadPlot(ng.net)
    plp.force_evolve_layout(1)
    sp.plp = plp

# for nd in sp.net.nodes():
#     sp.set_node_cap(nd, int(d+0.5+random.random()))

sp.relax(verbose=verbose)

sp.cascade_monitor = True

sp.drop_and_casc(n=iters, verbose=verbose, animate=animate)


name = str(ng) + "; " + str(sp)
print("SP info: " + name)
if not running_on_cluster:
    # sp.plot_cascade_measure_distrs(log_bins=True, error_split=1)
    # plt.legend()
    # sp.log_slope()
    # plt.legend(['slope $-\\frac{3}{2}$'])
    
    # sp.plot_avg_measure_vs_tot_sand()
    # plt.title(f'network {sp.ng}\n{sp}')
    # plt.legend()
    
    # plt.figure()
    # sp.log_slope(0.67,offset=1.3, color='red')
    # sp.scatter_tie_vs_size()
    # plt.legend()

    sp_just_in_case = sp
    pass

# save if on cluster
if running_on_cluster:
    pass
