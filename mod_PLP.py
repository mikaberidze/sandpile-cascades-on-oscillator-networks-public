#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 14 23:34:03 2021

@author: guga
"""

from tqdm import tqdm
from mod_SP import Sandpile
from mod_NG import NetworkGenerator
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import matplotlib.patches as mpatches
from PIL import Image, ImageDraw
import PIL
from matplotlib.figure import Figure
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import numpy as np
import scipy
import random
import itertools
import networkx as nx
import random as rnd


class PhaseLoadPlot:
    node_r = 0.3
    phase_ball_r = 0.08
    load_r = 0.08
    bar_hight = 0.2 # fraction of canvas
    lw = 1
    edge_color = "lightgray"
    node_color = "skyblue"
    phase_color = 'lightgreen'
    node_fill = True
    
    def __init__(self, net):
        self.net = net
        self.edge_colors = {e:PhaseLoadPlot.edge_color for e in net.edges()}
        self.special_loads = {}
        self.N = net.number_of_nodes()
        self.borders = [[0,1.2*self.N**.5],[0,1.2*self.N**.5]]
        self.layout = {}
        self.random_layout()
        self.loads = None
        self.phases = None
        self.title = None
        self.save_frames = True
        self.frames = [] #frames to export animation later
        self.bars = {}
        self.bar_colors = {}
    
    def color_edges(self, edges, color):
        for e in edges:
            self.edge_colors[e] = color
    
    def reset_edge_colors(self):
        self.color_edges(self.net.edges(), PhaseLoadPlot.edge_color)
    
    def add_special_loads(self, nd, s=1):
        self.special_loads[nd] = s
    
    def reset_special_loads(self):
        self.special_loads = {}
    
    def plot_2d(self):
        self.plot_carcass_2d()
        if self.loads is not None:
            self.add_loads_2d(self.loads)
        if self.phases is not None:
            self.add_phase_satelites_2d(self.phases)
        if self.title is not None:
            self.display_lable_2d(self.title)
        self.plot_bars_2d()
        plt.pause(0.01)
        if self.save_frames:
            self.pull_canvas_frame_2d()
        
    def display_lable_2d(self, label):
        ax = self.get_2d_axis()
        ax.set_title(label)
    
    def add_loads_2d(self, loads):
        ax = self.get_2d_axis()
        def draw_one_unit(x, y, r, special=False):
            if special:
                star(x,y,r)
            cir1 = plt.Circle((x, y), r, color='yellow')
            cir2 = plt.Circle((x, y), r, color='red' if special else 'brown', 
                              fill=False, lw=PhaseLoadPlot.lw)
            ax.add_patch(cir1)
            ax.add_patch(cir2)
        def star(x, y, r):
            n = 7
            r1 = r
            r2 = 2*r
            pts = []
            for i in range(2*n):
                th = i*np.pi/n
                r = r1 if i%2==0 else r2
                pts.append([x+r*np.cos(th), y+r*np.sin(th)])
            
            st = mpatches.Polygon(pts,  fc='Red')
            ste = mpatches.Polygon(pts,  fc='Red')
            ax.add_patch(st)
            ax.add_patch(ste)
            
        for nd in self.net.nodes():
            s = loads[nd]
            x, y = self.layout[nd]
            r = PhaseLoadPlot.load_r
            if s==1:
                special = nd in self.special_loads
                draw_one_unit(x,y,r,special)
            if s>1:
                R = r + s*r/np.pi
                for i in range(s):
                    x1 = x+R*np.cos(2*np.pi*i/s)
                    y1 = y+R*np.sin(2*np.pi*i/s)
                    special = nd in self.special_loads and \
                        i<self.special_loads[nd] 
                    draw_one_unit(x1,y1,r,special)
                
    def add_phase_satelites_2d(self, phases):
        ax = self.get_2d_axis()
        for nd in self.net.nodes():
            phi = phases[nd]
            x, y = self.layout[nd]
            x += PhaseLoadPlot.node_r*np.cos(phi)
            y += PhaseLoadPlot.node_r*np.sin(phi)
            cir1 = plt.Circle((x, y), PhaseLoadPlot.phase_ball_r, 
                             color=PhaseLoadPlot.phase_color)
            cir2 = plt.Circle((x, y), PhaseLoadPlot.phase_ball_r, 
                             color='black', fill=False, lw=PhaseLoadPlot.lw)
            ax.add_patch(cir1)
            ax.add_patch(cir2)
            # ax.(x, y, 0, c="white", edgecolors='black', s=8)
    
    def plot_bars_2d(self): #TODO!
        ax = self.get_2d_axis()
        ((x0,x1),(y0,y1)) = self.borders 
        hight = (y1-y0)/5
        width = hight/5
        offset = width*1.1
        for lbl in self.bars:
            val = self.bars[lbl]
            color = self.bar_colors[lbl] if lbl in self.bar_colors \
                else "grey"
            a = x1 - offset
            b = y1 - hight*1.1
            h = hight*val
            rect1 = mpatches.Rectangle((a,b),width,h, 
                        fill = True,
                        color = color,
                        linewidth = 1)
            rect2 = mpatches.Rectangle((a,b),width,hight, 
                        fill = False,
                        color = "grey",
                        linewidth = 1)
            plt.text(a+width/2,b-hight/10,lbl,horizontalalignment='center', \
                     verticalalignment="top")
            ax.add_patch(rect1)
            ax.add_patch(rect2)
            offset += width*2
    
    def include_load_saturation(self, load):
        ''' scaled full load to be represented as a bar '''
        self.bars['S'] = load
        self.bar_colors['S'] = 'yellow'
        
    def include_order_param(self, load):
        ''' r to be represented as a bar '''
        self.bars['r'] = load
        self.bar_colors['r'] = PhaseLoadPlot.phase_color
    
    def plot_carcass_2d(self):
        ax = self.get_2d_axis()
        ax.clear()
        for edge in self.net.edges():
            self.draw_edge_2d(ax, edge, color=self.edge_colors[edge])
        plt.show()
        for node in self.net.nodes():
            self.draw_node_2d(ax, node)
        plt.show()
        ax.axis("off")
        ax.set_aspect('equal')
        if self.bars: #make sure bars are visible
            ax.set_xlim([ax.get_xlim()[0], max(ax.get_xlim()[1],self.borders[0][1])])
            ax.set_ylim([ax.get_ylim()[0], max(ax.get_ylim()[1],self.borders[1][1])])
            
    def draw_edge_2d(self, ax, edge, color="lightgray", linewidth=2):
        v0 = np.array(self.layout[edge[0]])
        v1 = np.array(self.layout[edge[1]])
        e = (v0-v1)
        e /= np.linalg.norm(e)
        v0 -= e*PhaseLoadPlot.node_r
        v1 += e*PhaseLoadPlot.node_r
        
        ax.plot([v0[0], v1[0]], [v0[1], v1[1]], '-', color=color, 
                linewidth=linewidth, zorder=-1000)
    
    def draw_node_2d(self, ax, node):
        x, y = self.layout[node]
        
        if PhaseLoadPlot.node_fill:
            cir1 = plt.Circle((x, y), PhaseLoadPlot.node_r, color=\
                              PhaseLoadPlot.node_color)
            ax.add_patch(cir1)
        cir2 = plt.Circle((x, y), PhaseLoadPlot.node_r, color='k',
                          lw=PhaseLoadPlot.lw, fill=False)
        ax.add_patch(cir2)
        # ax.plot(x, y, 'o', color=color)
        
    def get_2d_axis(self):
        if hasattr(self, "ax_2d"):
            return self.ax_2d
        self.ax_2d = plt.subplot()
        return self.ax_2d   
    
    def random_layout(self):
        ''' randomly scatter the nodes in the canvas '''
        for node in self.net.nodes():
            xmin, xmax = self.borders[0]
            ymin, ymax = self.borders[1]
            self.layout[node] = [xmin+(xmax-xmin)*rnd.random(),
                                 ymin+(ymax-ymin)*rnd.random()]
        return self.layout
    
    def force_evolve_layout(self, t=10, dt=0.1, atol=0.1,
                            animate=False):
        ''' implements forced balance representation of the graph '''
        k = 1 # Hooks law
        c = 1 # Coulombs law
        n = 5 # wall reaction force
        
        nodes = list(self.layout)
        node_index_map = {nd:i for (i,nd) in enumerate(nodes)}
        xy = np.array(list(self.layout.values()))
        shapexy = xy.shape
        sizexy = shapexy[0]*shapexy[1]
        def neighbors(i):
            nd = nodes[i]
            ngbrs = self.net.neighbors(nd)
            return [node_index_map[ngbr] for ngbr in ngbrs]
        
        def dist(xy0, xy1):
            return ((xy0[0]-xy1[0])**2+(xy0[1]-xy1[1])**2)**.5
        
        def force(t, xy_flat):
            xy = np.reshape(xy_flat, shapexy)
            forces = [np.array([0.,0.]) for i in range(len(nodes))]
            for i in range(len(nodes)):
                jj = neighbors(i)
                
                # attraction
                for j in jj:
                    forces[i] += k*(xy[j]-xy[i])
                    
                # repulsion
                for j in range(len(nodes)):
                    if i!=j:
                        forces[i] -= c*(xy[j]-xy[i])/dist(xy[j],xy[i])**3
                
                # borders
                x, y = xy[i]
                xmin, xmax = self.borders[0]
                ymin, ymax = self.borders[1]
                r = PhaseLoadPlot.node_r
                N = np.array([0., 0.])
                    # square borders
                # if x-3*r<xmin: N[0] += n*(xmin-x+3*r)**2
                # if x+3*r>xmax: N[0] -= n*(x-xmax+3*r)**2
                # if y-3*r<ymin: N[1] += n*(ymin-y+3*r)**2
                # if y+3*r>ymax: N[1] -= n*(y-ymax+3*r)**2
                    # circular
                center = np.array([xmin+xmax, ymin+ymax])/2
                R = (xmax-xmin)/2
                if np.linalg.norm(xy[i]-center)+3*r > R:
                    N += n*(center-xy[i])/np.linalg.norm(center-xy[i])*\
                        (np.linalg.norm(xy[i]-center)+3*r-R)**2
                
                
                # away from corner
                corner = [self.borders[0][1], self.borders[1][1]]
                R = (ymax - ymin)*PhaseLoadPlot.bar_hight*2
                if self.bars and \
                    np.linalg.norm(xy[i]-corner) < R:
                        N += n*(xy[i]-corner)/np.linalg.norm(xy[i]-corner)*\
                            (R-np.linalg.norm(xy[i]-corner))**2
                        
                        
                forces[i] += N
                    
                
            return np.reshape(forces, sizexy)
        
        for i in tqdm(range(int(t/dt))):
            xy = np.array(list(self.layout.values()))
            sol = solve_ivp(force, [0, dt], np.reshape(xy, sizexy), atol=atol)
            xy_final = np.reshape(np.transpose(sol.y)[-1], shapexy)
            self.layout = {nodes[i]:list(xy_final[i]) 
                               for i in range(len(nodes))}
                    
            if animate:
                self.get_2d_axis().clear()
                self.plot_carcass_2d()
                plt.pause(0.01)

    def interactive_force_layout(self, t_0=1):
        t = t_0
        atol = 0.2
        while True:
            self.force_evolve_layout(t, animate=True, atol=atol)
            value = None
            while value is None:
                value = input(f"Should we continue balancing it? (current tolerance:{atol}) \n"\
                              "int - continue \n"\
                              "enter - finish \n"\
                              "d - double tolerance \n"\
                              "h - half tolerance \n")
                if value=="d":
                    atol *= 2
                    value = None
                if value=="h":
                    atol /= 2
                    value = None
                if value!="" and not str(value).isdigit():
                    value = None
            if value=="":
                break
            else: t = int(value)

    def pull_canvas_frame_2d(self):
        fig = self.get_2d_axis().get_figure()
        canvas = FigureCanvas(fig)
        canvas.draw()
        image = PIL.Image.frombytes('RGB', 
                    fig.canvas.get_width_height(),fig.canvas.tostring_rgb())

        self.frames.append(image)

    def export_animation(self, name="out"):
        self.frames[0].save(f"Out/{name}.gif", save_all=True, 
                            append_images=self.frames[1:], loop=0, duration=150)

# N = 30

# ng = NetworkGenerator.random_regular(N, 3)
# plp = PhaseLoadPlot(ng.net)
# loads = {nd:rnd.randint(0,4) for nd in plp.net.nodes()}
# phases = {nd:rnd.random()*2*np.pi for nd in plp.net.nodes()}
# plp.loads = loads
# plp.title = 'some title'
# plp.phases = phases
# plp.include_order_param(0.3)
# plp.include_load_saturation(0.9)
# plp.add_special_loads(0,1)
# plp.add_special_loads(1,3)
# plp.force_evolve_layout(1)
# plp.interactive_force_layout()
# plp.plot_2d()
# plp.export_animation()

# print(plp.borders)
