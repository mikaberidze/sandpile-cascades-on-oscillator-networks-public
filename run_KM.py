#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 13:54:49 2020

@author: guga
"""
from mod_KM import Kuramoto
from mod_NG import NetworkGenerator
import numpy as np
import matplotlib.pyplot as plt
import copy


# ------------- Complete graph test critical coupling ------------------------
def crit_coupling_complete(del_w, n):
    g0 = 1/(del_w*(2*np.pi)**0.5)
    return 2/(np.pi*n*g0)


def approx_crit_coupling(net, del_w):
    t = 30
    t_trust = 5
    w0 = 0

    ktheo = crit_coupling_complete(del_w, net.number_of_nodes())

    k_num = 6  # number of values of different k to test
    runs_per_k = 10
    kk = np.linspace(ktheo*0.6, ktheo*1.4, k_num)
    rr = [0]*k_num
    ee = [0]*k_num

    for i in range(k_num):
        rrr = [0]*runs_per_k
        for j in range(runs_per_k):
            km = Kuramoto(net, k=kk[i])
            km.w_normal_distr(w0, del_w)
            km.init_phase_uniform_distr()

            km.run(t)
            rrr[j] += km.get_avg_order_param(t_trust)

        rr[i] = np.average(rrr)
        ee[i] = np.std(rrr)

    # plt.errorbar(kk, rr, yerr=ee, fmt='o-')
    # plt.pause(0.1)

    r_threshold = 0.5
    j = -1
    for i in range(k_num):
        if rr[i] >= r_threshold:
            j = i
            break
    if j < 1:
        raise Exception("need to extend the range of tested couplings")

    w1 = (r_threshold - rr[j-1]) / (rr[j] - rr[j-1])  # weights
    w2 = (rr[j] - r_threshold) / (rr[j] - rr[j-1])
    k = kk[j-1]*w1 + kk[j]*w2

    slope = (rr[j]-rr[j-1]) / (kk[j]-kk[j-1])

    del_k = (ee[j-1] + ee[j])/slope
    return k, del_k


def complete_graph_crit_coupling_test():
    n = 30
    net = NetworkGenerator.complete(n).net

    del_w_min = 0.1
    del_w_max = 2
    del_w_num = 10

    del_ww = np.linspace(del_w_min, del_w_max, num=100)
    kk_theo = [crit_coupling_complete(del_w, n) for del_w in del_ww]
    plt.plot(del_ww, kk_theo)
    plt.pause(0.1)

    del_ww = np.linspace(del_w_min, del_w_max, num=del_w_num)
    kk = [0]*del_w_num
    ee = [0]*del_w_num

    for i in range(del_w_num):
        kk[i], ee[i] = approx_crit_coupling(net, del_ww[i])
        print(str(i+1) + ' out of ' + str(del_w_num))

    plt.errorbar(del_ww, kk, yerr=ee, fmt='o')
    plt.pause(0.1)

    plt.legend(['theoretical', 'simulation'])
    plt.xlabel('STD of frequency distribution')
    plt.ylabel('critical coupling')


# ----------------- ring network topology of solutions -----------------------
def stable_winding_number():
    n = 127
    m = 31
    w0 = 0
    del_w = 0.000

    net = NetworkGenerator.ring(n).net

    km = Kuramoto(net)

    km.w_normal_distr(w0, del_w)
    km.circularly_wrap_init_phases(winding_number=m, std=0.00)

    km.run(1800)
    km.animate(n=200)


# -------------------------- watch instability -------------------------------
def just_run_and_watch():
    n = 60
    del_w = 0
    w0 = 0
    kcrit = crit_coupling_complete(2, n)
    
    
    net = NetworkGenerator.complete(n).net

    km = Kuramoto(net)#, k=kcrit*1.)
    km.k = kcrit

    km.w_normal_distr(w0, del_w)
    km.init_phase_uniform_distr()

    km.run(10)
    km.animate(n=300)

# just_run_and_watch()

# -------------------------- watch instability -------------------------------
def watch_k():
    n = 30
    del_w = 2
    w0 = 0
    kcrit = crit_coupling_complete(2, n)

    net = NetworkGenerator.complete(n).net

    km = Kuramoto(net)

    km.w_normal_distr(w0, del_w)
    km.init_phase_uniform_distr()

    print(kcrit)
    k0 = kcrit*0.4
    k1 = kcrit*1.5
    kk = np.linspace(k0, k1, num=10)
    del_t = 15

    for i, k in enumerate(kk):
        print("k="+str(k))
        km.k = k
        km.run(del_t)
        km.animate(tmin=del_t*i, n=300)

# watch_k()

# ---------------------- toying with reversibility ---------------------------
def reverse_watch():
    n = 200
    del_w = 1
    w0 = 0
    kcrit = crit_coupling_complete(del_w, n)

    net = NetworkGenerator.complete(n).net

    km = Kuramoto(net, k=kcrit*1.5)
    km.k = kcrit*0.2

    km.w_normal_distr(w0, del_w)
    km.init_phase_uniform_distr(0.2*np.pi)

    t1 = 10
    t2 = 20

    km.run(t1)

    km.flip_time()
    km.run(t2)

    km.animate(n=300)
    
    
# ---------------------- Test phase lockings ---------------------------
def final_states():
    n = 200
    del_w = 3
    w0 = 0
    k = 10
    d = 3
    t = 100
    verbose = False
    
    parallel = 10
    
    net = NetworkGenerator.random_regular(n, d)

    km = Kuramoto(net)
    km.k = k

    km.w_normal_distr(w0, del_w)
    kms = [0]*parallel
    for i in range(parallel):
        kms[i] = copy.deepcopy(km)
    
    for km in kms:
        km.init_phase_uniform_distr()
        km.run(t)
        if km == kms[0] and verbose:
            km.animate(n=300)
    
    for km in kms:
        km.plot_relative_phases_vs_t([1,2,3,100,101,102,151,155,166,170])
        
    name = str(net)+" "+str(kms[0])
    plt.title(name)
    
    

